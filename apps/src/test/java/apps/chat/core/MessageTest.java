package apps.chat.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MessageTest {
    private Message message;
    private String messageString;
    private String name;

    /**
     * set up new message.
     */
    @BeforeEach
    public void setUp() {
        messageString = "hai";
        name = "fulan";

        message = new Message();
        message.setName(name);
        message.setMessage(messageString);
    }

    @Test
    public void testMethodGetName() {
        String name2 = message.getName();
        String messageString2 = message.getMessage();

        assertEquals(name, name2);
        assertEquals(messageString, messageString2);
    }
}