package apps.room.validator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import apps.room.core.Room;
import apps.room.core.RoomWithPassword;
import apps.room.core.base.SecuredRoom;
import apps.room.repository.RoomRepository;
import apps.user.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class EnterRoomWithValidatorTest {
    private EnterRoomWithValidator enterRoomWithValidator;
    private RoomRepository roomRepository;

    /**
     * reset for every test.
     */
    @BeforeEach
    public void setUp() {
        roomRepository = new RoomRepository();
        EnterRoomWithValidator.setRoomRepository(roomRepository);
        enterRoomWithValidator = new EnterRoomWithValidator();
    }

    @Test
    public void testSetAndGetRoomId() {
        enterRoomWithValidator.setRoomId(123);

        assertEquals(123, enterRoomWithValidator.getRoomId());
    }

    @Test
    public void testEnter() {
        Room room = new Room(123, "ini title");
        SecuredRoom securedRoom = new RoomWithPassword(room, "password");
        roomRepository.addRoom(securedRoom);

        enterRoomWithValidator.setRoomId(123);
        enterRoomWithValidator.setPassword("password");

        User user = new User();
        enterRoomWithValidator.enter(user);

        Room actualRoom = enterRoomWithValidator.getRoom();
        
        assertEquals(room, actualRoom);
    }

    @Test
    public void testEnterRoomDoesntExist() {
        Room room = new Room(123, "ini title");
        SecuredRoom securedRoom = new RoomWithPassword(room, "password");
        roomRepository.addRoom(securedRoom);

        enterRoomWithValidator.setRoomId(124);
        enterRoomWithValidator.setPassword("password");

        User user = new User();
        enterRoomWithValidator.enter(user);

        Room actualRoom = enterRoomWithValidator.getRoom();
        
        assertNull(actualRoom);
        assertEquals("Room doesn't exist", enterRoomWithValidator.getError());
    }

    @Test
    public void testEnterRoomWrongPassword() {
        Room room = new Room(123, "ini title");
        SecuredRoom securedRoom = new RoomWithPassword(room, "password");
        roomRepository.addRoom(securedRoom);

        enterRoomWithValidator.setRoomId(123);
        enterRoomWithValidator.setPassword("passwrd");

        User user = new User();
        enterRoomWithValidator.enter(user);

        Room actualRoom = enterRoomWithValidator.getRoom();
        
        assertNull(actualRoom);
        assertEquals("Password doesn't match", enterRoomWithValidator.getError());
    }

}