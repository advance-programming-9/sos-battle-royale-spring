package apps.room.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import apps.room.core.Room;
import apps.room.core.RoomWithoutPassword;
import apps.room.core.base.SecuredRoom;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class RoomRepositoryTest {
    private RoomRepository roomRepository;
    private Room room;
    private SecuredRoom securedRoom;

    /**
     * reset for every test.
     */
    @BeforeEach
    public void setUp() {
        room = new Room(123, "mantap");
        securedRoom = new RoomWithoutPassword(room);
        roomRepository = new RoomRepository();
    }

    @Test
    public void testGetRooms() {
        List<SecuredRoom> rooms = new ArrayList<>();

        SecuredRoom securedRoom = new RoomWithoutPassword(new Room());
        roomRepository.addRoom(securedRoom);
        rooms.add(securedRoom);

        List<SecuredRoom> actualRooms = roomRepository.getRooms();

        assertEquals(rooms, actualRooms);
    }

    @Test
    public void testGetRoomById() {
        roomRepository.addRoom(securedRoom);

        SecuredRoom actualsecuredRoom = roomRepository.getRoomById(123);

        assertEquals(securedRoom, actualsecuredRoom);
    }

    @Test
    public void testDeleteRoomById() {
        roomRepository.addRoom(securedRoom);

        roomRepository.deleteRoomById(123);
        SecuredRoom actualsecuredRoom = roomRepository.getRoomById(123);

        assertNull(actualsecuredRoom);
    }

}