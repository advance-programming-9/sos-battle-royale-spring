package apps.room.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import apps.bot.repository.BotRepository;
import apps.room.core.CreateRoom;
import apps.room.core.RequestRoomId;
import apps.room.core.Room;
import apps.room.core.RoomResponse;
import apps.room.core.RoomWithPassword;
import apps.room.core.base.SecuredRoom;
import apps.room.repository.RoomRepository;
import apps.room.service.RoomImpl;
import apps.room.validator.EnterRoomWithValidator;
import apps.user.auth.JWT;
import apps.user.model.User;
import apps.user.repository.MatchRepository;
import apps.user.repository.UserRepositoryJPA;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = RoomController.class)
public class RoomControllerTest {
    ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    public MockMvc mockMVC;

    @MockBean
    private RoomImpl service;

    @MockBean
    private JWT jwt;

    @MockBean
    private RoomRepository repo;

    @MockBean
    private UserRepositoryJPA userRepo;

    @MockBean
    private MatchRepository matchRepo;

    @MockBean
    private BotRepository botRepository;

    private String token;

    private User user;

    /**
     * setting up test.
     */
    @BeforeEach
    public void setUp() {
        token = "asd";
        user = new User(123L);
        user.setUsername("fulan");
    }

    @Test
    public void testFindAll() throws Exception {
        when(jwt.validateToken(token)).thenReturn(true);
        mockMVC.perform(
                get("/room")
                .header("Authorization", "Bearer " + token)
        )
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andDo(result ->
                assertEquals("[]",result.getResponse().getContentAsString())
            );
    }

    @Test
    public void testInvalidFindAll() throws Exception {
        when(jwt.validateToken(token)).thenReturn(false);
        mockMVC.perform(
                get("/room")
                .header("Authorization", "Bearer " + token)
        )
            .andExpect(status().isBadRequest())
            .andDo(result ->
                    assertEquals("-1",result.getResponse().getContentAsString())
            );
    }

    @Test
    public void testExitInvalid() throws Exception {
        RequestRoomId requestRoomId = new RequestRoomId();
        requestRoomId.setRoomId(42);

        String jsonRequest = objectMapper.writeValueAsString(requestRoomId);

        when(jwt.validateToken(token)).thenReturn(false);
        when(jwt.getUsernameFromToken(token)).thenReturn("fulan");
        mockMVC.perform(
                post("/room/exit")
                .header("Authorization", "Bearer " + token)
                .content(jsonRequest)
                .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isBadRequest())
            .andDo(result ->
                    assertEquals("-1",result.getResponse().getContentAsString())
            );
    }

    @Test
    public void testExitValid() throws Exception {
        RequestRoomId requestRoomId = new RequestRoomId();
        requestRoomId.setRoomId(42);
        int idRoom = requestRoomId.getRoomId();

        String jsonRequest = objectMapper.writeValueAsString(requestRoomId);

        when(jwt.validateToken(token)).thenReturn(true);
        when(jwt.getUsernameFromToken(token)).thenReturn("fulan");
        when(service.delete(idRoom,jwt.getUsernameFromToken(token))).thenReturn(true);
        mockMVC.perform(
                post("/room/exit")
                .header("Authorization", "Bearer " + token)
                .content(jsonRequest)
                .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isOk())
            .andDo(result ->
                    assertEquals("42",result.getResponse().getContentAsString())
            );
    }

    @Test
    public void testExitFailDelete() throws Exception {
        RequestRoomId requestRoomId = new RequestRoomId();
        requestRoomId.setRoomId(42);
        int idRoom = requestRoomId.getRoomId();

        String jsonRequest = objectMapper.writeValueAsString(requestRoomId);

        when(jwt.validateToken(token)).thenReturn(true);
        when(jwt.getUsernameFromToken(token)).thenReturn("fulan");
        when(service.delete(idRoom,jwt.getUsernameFromToken(token))).thenReturn(false);
        mockMVC.perform(
                post("/room/exit")
                .header("Authorization", "Bearer " + token)
                .content(jsonRequest)
                .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isBadRequest())
            .andDo(result ->
                    assertEquals("-1",result.getResponse().getContentAsString())
            );
    }

    @Test
    public void testCreateSuccess() throws Exception {
        CreateRoom roomComp = new CreateRoom();
        roomComp.setTitle("Test");
        roomComp.setPassword("uye123");
        roomComp.create();
        SecuredRoom room = roomComp.getRoom();
        int id = room.getRoomId();

        String jsonRequest = objectMapper.writeValueAsString(roomComp);

        when(jwt.validateToken(token)).thenReturn(true);
        when(jwt.getUsernameFromToken(token)).thenReturn("fulan");
        when(service.create(eq("fulan"),any(CreateRoom.class))).thenReturn(room);

        mockMVC.perform(
                put("/room/create")
                .header("Authorization", "Bearer " + token)
                .content(jsonRequest)
                .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andDo(result -> {
                    String content = result.getResponse().getContentAsString();
                    assertEquals(Integer.toString(id),content);
                });
    }

    @Test
    public void testCreateInvalid() throws Exception {
        CreateRoom roomComp = new CreateRoom();
        roomComp.setTitle("Test");
        roomComp.setPassword("uye123");
        roomComp.create();

        String jsonRequest = objectMapper.writeValueAsString(roomComp);
        when(jwt.validateToken(token)).thenReturn(false);

        mockMVC.perform(
            put("/room/create")
            .header("Authorization", "Bearer " + token)
            .content(jsonRequest)
            .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isBadRequest())
            .andDo(result ->
                    assertEquals("-1",result.getResponse().getContentAsString())
            );
    }

    @Test
    public void testEnterInvalid() throws Exception {
        EnterRoomWithValidator validator = new EnterRoomWithValidator();
        validator.setRoomId(42);
        validator.setPassword("123");
        EnterRoomWithValidator.setRoomRepository(repo);
        Room roomTest = new Room(42,"WAWA");
        SecuredRoom roomWithPass = new RoomWithPassword(roomTest,"123");
        when(repo.getRoomById(42)).thenReturn(roomWithPass);
        validator.enter(user);
        RoomResponse roomResp = new RoomResponse();
        roomResp.setError("INVALID_JWT");

        String jsonRequest = objectMapper.writeValueAsString(validator);
        when(jwt.validateToken(token)).thenReturn(false);

        mockMVC.perform(
                post("/room/enter")
                .header("Authorization", "Bearer " + token)
                .content(jsonRequest)
                .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andDo(result -> {
                    String content = result.getResponse().getContentAsString();
                    String error = JsonPath.read(content, "$.error");
                    assertEquals(roomResp.getError(), error);
                });
    }

    @Test
    public void testEnterFail() throws Exception {
        EnterRoomWithValidator val = new EnterRoomWithValidator();
        val.setRoomId(42);
        val.setPassword("123");
        EnterRoomWithValidator.setRoomRepository(repo);
        Room roomTest = new Room(42,"WAWA");
        SecuredRoom roomWithPass = new RoomWithPassword(roomTest,"123");
        when(repo.getRoomById(42)).thenReturn(roomWithPass);
        val.enter(user);
        RoomResponse resp = new RoomResponse();
        resp.setError("WRONG PASSWORD/ROOM FULL");

        String jsonRequest = objectMapper.writeValueAsString(val);

        when(jwt.validateToken(token)).thenReturn(true);
        when(jwt.getUsernameFromToken(token)).thenReturn("fulan");
        when(service.enter(eq("fulan"),any(resp.getClass()),any(val.getClass()))).thenReturn(resp);

        mockMVC.perform(
                post("/room/enter")
                        .header("Authorization", "Bearer " + token)
                        .content(jsonRequest)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andDo(result -> {
                    String content = result.getResponse().getContentAsString();
                    String error = JsonPath.read(content, "$.error");
                    assertNotNull(error);
                });
    }

    @Test
    public void testEnterSuccess() throws Exception {
        EnterRoomWithValidator val = new EnterRoomWithValidator();
        val.setRoomId(42);
        val.setPassword("123");
        EnterRoomWithValidator.setRoomRepository(repo);
        Room roomTest = new Room(42,"WAWA");
        SecuredRoom roomWithPass = new RoomWithPassword(roomTest,"123");
        when(repo.getRoomById(42)).thenReturn(roomWithPass);
        val.enter(user);

        Optional<User> userOptional = Optional.of(user);
        RoomResponse.setUserRepositoryJPA(userRepo,matchRepo,botRepository);
        when(matchRepo.getInGameScore(user.getId())).thenReturn((long)100);
        when(userRepo.findById(user.getId())).thenReturn(userOptional);

        RoomResponse resp = new RoomResponse();
        when(jwt.validateToken(token)).thenReturn(true);
        when(jwt.getUsernameFromToken(token)).thenReturn("fulan");
        when(service.enter(eq("fulan"),any(resp.getClass()),any(val.getClass()))).thenReturn(resp);

        String jsonRequest = objectMapper.writeValueAsString(val);;

        mockMVC.perform(
                post("/room/enter")
                .header("Authorization", "Bearer " + token)
                .content(jsonRequest)
                .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andDo(result -> {
                    String content = result.getResponse().getContentAsString();
                    String error = JsonPath.read(content, "$.error");
                    assertNull(error);
                });
    }
}

