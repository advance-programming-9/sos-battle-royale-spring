package apps.room.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import apps.room.core.CreateRoom;
import apps.room.core.Room;
import apps.room.core.RoomResponse;
import apps.room.core.RoomWithPassword;
import apps.room.core.RoomWithoutPassword;
import apps.room.core.base.SecuredRoom;
import apps.room.repository.RoomRepository;
import apps.room.validator.EnterRoomWithValidator;
import apps.scoreboard.core.Scoreboard;
import apps.user.model.User;
import apps.user.repository.MatchRepository;
import apps.user.repository.UserRepositoryJPA;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import org.springframework.messaging.simp.SimpMessagingTemplate;

@ExtendWith(MockitoExtension.class)
public class RoomImplTest {

    @Mock
    private UserRepositoryJPA userRepo;

    @Spy
    private RoomRepository roomRepo;

    @Mock
    private MatchRepository matchRepo;

    @Mock
    private Scoreboard scoreboard;

    @Mock
    private CreateRoom roomComp;

    @InjectMocks
    private RoomImpl service;

    private SecuredRoom roomWithPass;
    private SecuredRoom roomWithoutPass;
    private EnterRoomWithValidator enterRoom;

    @Mock
    private SimpMessagingTemplate simpMessagingTemplate;
    private User user;

    /**
     * setting up test.
     */
    @BeforeEach
    public void setUp() {
        Room roomTest = new Room(1,"WAWA");
        roomWithPass = new RoomWithPassword(roomTest,"123");
        roomWithoutPass = new RoomWithoutPassword(roomTest);
        user = new User(321);
        enterRoom = new EnterRoomWithValidator();
        enterRoom.setRoomId(1);
        enterRoom.setPassword("123");
    }

    @Test
    public void findAllTest() {
        List<SecuredRoom> rooms = service.findAll();
        assertEquals(ArrayList.class, rooms.getClass());
    }

    @Test
    public void createTest() {
        doNothing().when(roomComp).create();
        when(roomComp.getRoom()).thenReturn(roomWithoutPass);
        when(userRepo.findByUsername("fulan")).thenReturn(Optional.of(new User(123L)));
        doAnswer((Answer<Void>) invocation -> null).when(roomRepo).addRoom(roomWithoutPass);
        SecuredRoom roomRes = service.create("fulan", roomComp);
        assertEquals(roomRes,roomWithoutPass);
    }
    
    @Test
    public void deleteTestSuccess() {
        roomWithPass.enter(user,"123");
        when(userRepo.findByUsername("fulan")).thenReturn(java.util.Optional.ofNullable(user));
        when(roomRepo.getRoomById(1)).thenReturn(roomWithPass);
        when(matchRepo.getInGameScore((long) 321)).thenReturn((long)100);
        doNothing().when(matchRepo).removeMatchId((long)321);
        doNothing().when(scoreboard).update(user);

        boolean result = service.delete(1,"fulan");
        assertEquals(100,user.getScore());
        verify(simpMessagingTemplate, times(0)).convertAndSend(
            "/game/room/" + roomWithPass.getRoomId(),
            roomWithPass.getRoomResponse()
        );
        assertTrue(result);
    }

    @Test
    public void deleteTestFailnoRoom() {
        when(userRepo.findByUsername("fulan")).thenReturn(java.util.Optional.ofNullable(user));
        when(roomRepo.getRoomById(1)).thenReturn(null);
        boolean result = service.delete(1,"fulan");
        verify(simpMessagingTemplate, times(0)).convertAndSend(
                "/game/room/" + 1,roomWithPass.getRoomResponse()
        );
        assertFalse(result);
    }

    @Test
    public void deleteTestFailNoUserQuit() {
        when(userRepo.findByUsername("fulan")).thenReturn(java.util.Optional.ofNullable(user));
        when(roomRepo.getRoomById(1)).thenReturn(roomWithPass);
        boolean result = service.delete(1,"fulan");
        verify(simpMessagingTemplate, times(0)).convertAndSend(
                "/game/room/" + 1,roomWithPass.getRoomResponse()
        );
        assertFalse(result);
    }

    @Test
    public void enterTestSuccessNotInMatch() {
        when(roomRepo.getRoomById(1)).thenReturn(roomWithPass);
        enterRoom.enter(user);
        RoomResponse roomResp = new RoomResponse();
        when(userRepo.findByUsername("fulan")).thenReturn(java.util.Optional.ofNullable(user));
        when(matchRepo.isInAMatch(user.getId())).thenReturn(false);
        when(matchRepo.isInThisMatch(user.getId(),1)).thenReturn(false);
        RoomResponse result = service.enter("fulan",roomResp,enterRoom);
        assertNull(result.getError());
    }

    @Test
    public void enterTestFailInThisMatch() {
        when(roomRepo.getRoomById(1)).thenReturn(roomWithPass);
        enterRoom.enter(user);
        RoomResponse roomResp = new RoomResponse();
        when(userRepo.findByUsername("fulan")).thenReturn(java.util.Optional.ofNullable(user));
        when(matchRepo.isInAMatch(user.getId())).thenReturn(false);
        when(matchRepo.isInThisMatch(user.getId(),1)).thenReturn(true);
        RoomResponse result = service.enter("fulan",roomResp,enterRoom);
        assertNull(result.getError());
    }

    @Test
    public void enterTestFailIsInMatch() {
        when(roomRepo.getRoomById(1)).thenReturn(roomWithPass);
        enterRoom.enter(user);
        RoomResponse roomResp = new RoomResponse();
        when(userRepo.findByUsername("fulan")).thenReturn(java.util.Optional.ofNullable(user));
        when(matchRepo.isInAMatch(user.getId())).thenReturn(true);
        when(matchRepo.isInThisMatch(user.getId(),1)).thenReturn(false);
        RoomResponse result = service.enter("fulan",roomResp,enterRoom);
        assertEquals("IS_IN_A_MATCH",result.getError());
    }

    @Test
    public void enterTestFailsNoRoom() {
        enterRoom.enter(user);
        RoomResponse roomResp = new RoomResponse();
        when(userRepo.findByUsername("fulan")).thenReturn(java.util.Optional.ofNullable(user));
        doNothing().when(matchRepo).removeMatchId((long)321);
        RoomResponse result = service.enter("fulan",roomResp,enterRoom);
        assertEquals("Room doesn't exist",result.getError());
    }

    @Test
    public void enterTestFailsWrongPass() {
        enterRoom.setPassword("aselole");
        when(roomRepo.getRoomById(1)).thenReturn(roomWithPass);
        RoomResponse roomResp = new RoomResponse();
        when(userRepo.findByUsername("fulan")).thenReturn(java.util.Optional.ofNullable(user));
        when(matchRepo.isInAMatch(user.getId())).thenReturn(false);
        when(matchRepo.isInThisMatch(user.getId(),1)).thenReturn(false);
        RoomResponse result = service.enter("fulan",roomResp,enterRoom);
        assertEquals("WRONG_PASSWORD/ROOM_FULL",result.getError());
    }
}
