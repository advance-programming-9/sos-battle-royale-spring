package apps.room.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class RequestRoomIdTest {
    private RequestRoomId requestRoomId;

    @BeforeEach
    public void setUp() {
        requestRoomId = new RequestRoomId();
    }

    @Test
    public void testSetAndGetRoomId() {
        requestRoomId.setRoomId(123);

        assertEquals(123, requestRoomId.getRoomId());
    }

}