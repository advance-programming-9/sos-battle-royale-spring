package apps.room.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

import apps.bot.repository.BotRepository;
import apps.scoreboard.core.adapter.SafeUser;
import apps.user.model.User;
import apps.user.repository.MatchRepository;
import apps.user.repository.UserRepositoryJPA;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class RoomResponseTest {
    private RoomResponse roomResponse;
    private Room room;
    private User user;
    private MatchRepository matchRepository;
    private BotRepository botRepository;

    @Mock
    private UserRepositoryJPA userRepositoryJPA;


    /**
     * reset for every test.
     */
    @BeforeEach
    public void setUp() {
        matchRepository = new MatchRepository();
        RoomResponse.setUserRepositoryJPA(userRepositoryJPA, matchRepository, botRepository);
        roomResponse = new RoomResponse();
        room = new Room();
        roomResponse.setRoom(room);
        user = new User(100L);
        user.setUsername("fulan");
    }

    @Test
    public void testSetAndGetError() {
        roomResponse.setError("error");

        assertEquals("error", roomResponse.getError());

        roomResponse = new RoomResponse("error2");

        assertEquals("error2", roomResponse.getError());
    }

    @Test
    public void testIdZeroRoomMaster() {
        user = new User();

        room.enter(user);

        assertNull(roomResponse.getRoomMaster());
    }

    @Test
    public void testGetRoomMaster() {
        when(userRepositoryJPA.findById(100L)).thenReturn(Optional.of(user));

        room.enter(user);

        assertEquals("fulan", roomResponse.getRoomMaster());
    }

    @Test
    public void testGetCountPeople() {
        room.enter(user);
        
        assertEquals(1, roomResponse.getCountPeople());
    }

    @Test
    public void testGetState() {
        User user2 = new User(124);
        room.enter(user);
        room.enter(user2);
        assertEquals("waiting", roomResponse.getState());

        room.start();
        assertEquals("playing", roomResponse.getState());

        room.finish();
        assertEquals("finished", roomResponse.getState());
    }

    @Test
    public void testGetWhoIsNext() {
        User user2 = new User(101L);
        user2.setUsername("fulan2");

        when(userRepositoryJPA.findById(100L)).thenReturn(Optional.of(user));
        when(userRepositoryJPA.findById(101L)).thenReturn(Optional.of(user2));

        room.enter(user);
        room.enter(user2);
        room.start();

        assertEquals("fulan", roomResponse.getWhoIsNext());

        room.passTurn(user.getId());

        assertEquals("fulan2", roomResponse.getWhoIsNext());
    }

    @Test
    public void testGetWhoIsNextNull() {
        assertNull(roomResponse.getWhoIsNext());
    }

    @Test
    public void testGetUsers() {
        List<SafeUser> safeUsers = new ArrayList<>();

        safeUsers.add(new SafeUser(user));
        room.enter(user);

        when(userRepositoryJPA.findById(100L)).thenReturn(Optional.of(user));
        matchRepository.setMatch(100L, room.getRoomId());

        List<SafeUser> actualSafeUsers = roomResponse.getUsers();

        assertEquals(safeUsers.get(0).getUsername(), actualSafeUsers.get(0).getUsername());
    }

    @Test
    public void testGetSortedUsers() {
        User user2 = new User(101L);
        user2.setUsername("fulan2");

        when(userRepositoryJPA.findById(100L)).thenReturn(Optional.of(user));
        when(userRepositoryJPA.findById(101L)).thenReturn(Optional.of(user2));

        matchRepository.setMatch(100L, room.getRoomId());
        matchRepository.setMatch(101L, room.getRoomId());

        matchRepository.addInGameScore(100L, 100L);
        matchRepository.addInGameScore(101L, 150L);

        user.setScore(100L);
        user2.setScore(150L);

        room.enter(user);
        room.enter(user2);

        SafeUser actualSafeUser = roomResponse.getSortedUsersByInGameScore().get(0);

        assertEquals("fulan2", actualSafeUser.getUsername());
    }
}