package apps.room.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import apps.room.core.base.SecuredRoom;
import apps.user.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CreateRoomTest {
    private CreateRoom createRoom;
    private User user;

    /**
     * reset for every test.
     */
    @BeforeEach
    public void setUp() {
        createRoom = new CreateRoom();
        user = new User();
    }

    @Test
    public void testCreateRoomWithoutPassword() {
        createRoom.setTitle("title");
        createRoom.setPassword("");
        createRoom.create();

        SecuredRoom securedRoom = createRoom.getRoom();

        Room room = securedRoom.enter(user, "");

        assertNotNull(room);

        assertEquals("title", room.getTitle());
    }

    @Test
    public void testCreateRoomWithPassword() {
        createRoom.setTitle("title");
        createRoom.setPassword("password");
        createRoom.create();

        SecuredRoom securedRoom = createRoom.getRoom();

        Room room = securedRoom.enter(user, "");

        assertNull(room);

        room = securedRoom.enter(user, "password");

        assertNotNull(room);
    }

    @Test
    public void testCreateRoomNullTitleAndNullPassword() {
        createRoom.setTitle(null);
        createRoom.setPassword(null);
        createRoom.create();

        SecuredRoom securedRoom = createRoom.getRoom();

        Room room = securedRoom.enter(user, "");

        assertNotNull(room);
        assertEquals("default", room.getTitle());
    }

    @Test
    public void testCreateRoomEmptyTitleAndNullPassword() {
        createRoom.setTitle("");
        createRoom.setPassword(null);
        createRoom.create();

        SecuredRoom securedRoom = createRoom.getRoom();

        Room room = securedRoom.enter(user, "");

        assertNotNull(room);
        assertEquals("default", room.getTitle());
    }

    @Test
    public void testGetPassword() {
        createRoom.setPassword("passwordnya");

        assertEquals("passwordnya", createRoom.getPassword());
    }
}