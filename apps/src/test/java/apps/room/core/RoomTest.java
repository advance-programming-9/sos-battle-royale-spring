package apps.room.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import apps.user.model.User;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class RoomTest {
    private Room testRoom;
    private User testUser;

    @BeforeEach
    public void setUp() {
        testUser = new User(123L);
        testRoom = new Room(123456789,"Ini Test Game");
    }

    @Test
    public void testAddUserSuccess() {
        Room actualRoom = testRoom.enter(testUser);

        assertEquals(testRoom, actualRoom);
    }

    @Test
    public void testAddUserFail() {
        for (long i = 1; i <= 10; i++) {
            testRoom.enter(new User(i));
        }
        Room acrtualRoom = testRoom.enter(testUser);

        assertNull(acrtualRoom);
    }

    @Test
    public void testRemoveUserSuccess() {
        testRoom.enter(testUser);
        boolean status = testRoom.quit(testUser);

        assertTrue(status);
        assertEquals(0,testRoom.getCountPeople());
    }

    @Test
    public void testRemoveUserFail() {
        boolean status = testRoom.quit(testUser);

        assertFalse(status);
    }

    @Test
    public void testGetRoomMaster() {
        testRoom.enter(testUser);

        assertEquals(123,testRoom.getRoomMaster());
    }

    @Test
    public void testGetCountPeople() {
        testRoom.enter(testUser);

        assertEquals(1,testRoom.getCountPeople());
    }

    @Test
    public void testGetId() {
        assertEquals(123456789,testRoom.getRoomId());
    }

    @Test
    public void testGetTitle() {
        assertEquals("Ini Test Game",testRoom.getTitle());
    }

    @Test
    public void testGetIsProtected() {
        assertFalse(testRoom.getIsProtected());
    }

    @Test
    public void testEnter() {
        Room actualRoom = testRoom.enter(testUser);

        assertEquals(testRoom, actualRoom);
    }

    @Test
    public void testGetArena() {
        assertNotNull(testRoom.getArena());
    }

    @Test
    public void testGetWhoIsNext() {
        assertEquals(-1,testRoom.getWhoIsNext());
    }

    @Test
    public void testFinish() {
        User testUser2 = new User(124);
        testRoom.enter(testUser);
        testRoom.enter(testUser2);
        boolean status = testRoom.start();

        assertTrue(status);

        status = testRoom.finish();

        assertTrue(status);
    }

    @Test
    public void testGetUsers() {
        List<Long> users = new ArrayList<>();

        users.add(testUser.getId());
        testRoom.enter(testUser);
        
        List<Long> actualUsers = testRoom.getUsers();

        assertEquals(users, actualUsers);
    }
}
