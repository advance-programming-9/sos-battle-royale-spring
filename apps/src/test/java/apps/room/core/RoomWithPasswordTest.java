package apps.room.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import apps.game.core.arena.Arena;
import apps.game.core.arena.GridRequest;
import apps.room.core.base.SecuredRoom;
import apps.user.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class RoomWithPasswordTest {
    private SecuredRoom testSecuredRoom;
    private User testUser;
    private Room realTestRoom;

    /**
     * setup new test Room With Password, and new test User.
     */
    @BeforeEach
    public void setUp() {
        testUser = new User(123);
        realTestRoom = new Room(123456789,"Ini Test Game");
        testSecuredRoom = new RoomWithPassword(realTestRoom,"password");
    }

    @Test
    public void testEnterSuccess() {
        Room actualRoom = testSecuredRoom.enter(testUser,"password");

        assertEquals(realTestRoom, actualRoom);
    }

    @Test
    public void testEnterFail() {
        Room actualRoom = testSecuredRoom.enter(testUser,"pass");

        assertNull(actualRoom);
    }

    @Test
    public void testGetCountPeople() {
        testSecuredRoom.enter(testUser,"password");

        assertEquals(1,testSecuredRoom.getCountPeople());
    }

    @Test
    public void testGetId() {
        assertEquals(123456789, testSecuredRoom.getRoomId());
    }

    @Test
    public void testGetContent() {
        assertEquals("Ini Test Game",testSecuredRoom.getTitle());
    }

    @Test
    public void testGetIsProtected() {
        assertTrue(testSecuredRoom.getIsProtected());
    }

    @Test
    public void testQuit() {
        testSecuredRoom.enter(testUser, "password");

        assertEquals(1, testSecuredRoom.getCountPeople());

        testSecuredRoom.quit(testUser);

        assertEquals(0, testSecuredRoom.getCountPeople());
    }

    @Test
    public void testRoomResponse() {
        RoomResponse roomResponse = testSecuredRoom.getRoomResponse();

        assertNotNull(roomResponse);
    }

    @Test
    public void testRoomResponseAlwaysSame() {
        RoomResponse roomResponse = testSecuredRoom.getRoomResponse();

        assertEquals(roomResponse,testSecuredRoom.getRoomResponse());
    }

    @Test
    public void testStart() {
        User testUser2 = new User(124);
        testSecuredRoom.enter(testUser, "password");
        Room room = testSecuredRoom.enter(testUser2, "password");

        boolean status = testSecuredRoom.start();

        assertTrue(status);
        assertEquals(realTestRoom, room);
        assertEquals("playing", room.getState());
    }

    @Test
    public void testClick() {
        GridRequest gridRequest = new GridRequest();
        gridRequest.setCharacter("C");
        gridRequest.setX("0");
        gridRequest.setY("1");

        User testUser2 = new User(124);
        testSecuredRoom.enter(testUser, "password");
        testSecuredRoom.enter(testUser2, "password");
        testSecuredRoom.start();
        testSecuredRoom.click(gridRequest);

        RoomResponse roomResponse = testSecuredRoom.getRoomResponse();
        Arena arena = roomResponse.getArena();
        char character = arena.getStats(0, 1);

        assertEquals('C', character);
    }

    @Test
    public void testPassTurn() {
        testSecuredRoom.enter(testUser, "password");
        testSecuredRoom.start();
        testSecuredRoom.passTurn(testUser.getId());

        RoomResponse roomResponse = testSecuredRoom.getRoomResponse();

        assertNotNull(roomResponse);
    }

    @Test
    public void testEnterWrongMethodInvalid() {
        assertThrows(Exception.class, () -> 
            testSecuredRoom.enter(testUser)
        );
    }
}
