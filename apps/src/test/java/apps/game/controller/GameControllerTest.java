package apps.game.controller;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import apps.bot.repository.BotRepository;
import apps.room.core.RequestRoomId;
import apps.room.core.Room;
import apps.room.core.RoomResponse;
import apps.room.core.RoomWithoutPassword;
import apps.room.core.base.SecuredRoom;
import apps.room.repository.RoomRepository;
import apps.user.auth.JWT;
import apps.user.model.User;
import apps.user.repository.MatchRepository;
import apps.user.repository.UserRepositoryJPA;
import com.google.gson.Gson;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = GameController.class)
public class GameControllerTest {
    private final Gson gson = new Gson();

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RoomRepository roomRepository;

    @MockBean
    private UserRepositoryJPA userRepositoryJPA;

    @MockBean
    private BotRepository botRepository;

    @MockBean
    private MatchRepository matchRepository;

    @MockBean
    private SimpMessagingTemplate simpMessagingTemplate;

    @MockBean
    private JWT jwt;

    @Test
    public void testStartGameInvalidJwt() throws Exception {
        String token = "asd";

        User user = new User(123L);
        user.setUsername("fulan");

        SecuredRoom securedRoom = new RoomWithoutPassword(new Room(42, "ini title"));
        securedRoom.enter(user, "");

        when(jwt.validateToken(token)).thenReturn(false);
        when(roomRepository.getRoomById(42)).thenReturn(securedRoom);
        when(jwt.getUsernameFromToken(token)).thenReturn("fulan");
        when(userRepositoryJPA.findById(123L)).thenReturn(Optional.of(user));

        RequestRoomId requestRoomId = new RequestRoomId();
        requestRoomId.setRoomId(42);

        String jsonRequest = gson.toJson(requestRoomId);

        mockMvc.perform(
                post("/game/start")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonRequest)
                .header("Authorization", "Bearer " + token)
                .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(handler().methodName("startGame"));

        verify(simpMessagingTemplate, times(0)).convertAndSend(
            "/game/room/" + 42,
            securedRoom.getRoomResponse()
        );
    }

    @Test
    public void testStartGameRoomNotFound() throws Exception {
        String token = "asd";

        User user = new User(123L);
        user.setUsername("fulan");

        SecuredRoom securedRoom = new RoomWithoutPassword(new Room(42, "ini title"));
        securedRoom.enter(user, "");

        when(jwt.validateToken(token)).thenReturn(true);
        when(roomRepository.getRoomById(42)).thenReturn(null);
        when(jwt.getUsernameFromToken(token)).thenReturn("fulan");
        when(userRepositoryJPA.findById(123L)).thenReturn(Optional.of(user));

        RequestRoomId requestRoomId = new RequestRoomId();
        requestRoomId.setRoomId(42);

        String jsonRequest = gson.toJson(requestRoomId);

        mockMvc.perform(
                post("/game/start")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonRequest)
                .header("Authorization", "Bearer " + token)
                .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(handler().methodName("startGame"));

        verify(simpMessagingTemplate, times(0)).convertAndSend(
            "/game/room/" + 42,
            securedRoom.getRoomResponse()
        );
    }

    @Test
    public void testStartGameInvalidRoomMaster() throws Exception {
        String token = "asd";

        User user = new User(123L);
        user.setUsername("fulan");

        SecuredRoom securedRoom = new RoomWithoutPassword(new Room(42, "ini title"));
        securedRoom.enter(user, "");
        SecuredRoom securedRoomSpy = spy(securedRoom);

        RoomResponse roomResponse = securedRoom.getRoomResponse();

        when(jwt.validateToken(token)).thenReturn(true);
        when(roomRepository.getRoomById(42)).thenReturn(securedRoomSpy);
        when(jwt.getUsernameFromToken(token)).thenReturn("fulan2");
        when(securedRoomSpy.getRoomResponse()).thenReturn(roomResponse);
        when(userRepositoryJPA.findById(123L)).thenReturn(Optional.of(user));

        RequestRoomId requestRoomId = new RequestRoomId();
        requestRoomId.setRoomId(42);

        String jsonRequest = gson.toJson(requestRoomId);

        mockMvc.perform(
                post("/game/start")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonRequest)
                .header("Authorization", "Bearer " + token)
                .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(handler().methodName("startGame"));

        verify(simpMessagingTemplate, times(0)).convertAndSend(
            "/game/room/" + 42,
            roomResponse
        );
    }

    @Test
    public void testStartGameValid() throws Exception {
        String token = "asd";

        User user = new User(123L);
        user.setUsername("fulan");

        SecuredRoom securedRoom = new RoomWithoutPassword(new Room(42, "ini title"));
        securedRoom.enter(user, "");
        SecuredRoom securedRoomSpy = spy(securedRoom);

        RoomResponse roomResponse = securedRoom.getRoomResponse();

        when(jwt.validateToken(token)).thenReturn(true);
        when(roomRepository.getRoomById(42)).thenReturn(securedRoomSpy);
        when(jwt.getUsernameFromToken(token)).thenReturn("fulan");
        when(securedRoomSpy.getRoomResponse()).thenReturn(roomResponse);
        when(userRepositoryJPA.findById(123L)).thenReturn(Optional.of(user));

        RequestRoomId requestRoomId = new RequestRoomId();
        requestRoomId.setRoomId(42);

        String jsonRequest = gson.toJson(requestRoomId);

        mockMvc.perform(
                post("/game/start")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonRequest)
                .header("Authorization", "Bearer " + token)
                .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(handler().methodName("startGame"));

        verify(simpMessagingTemplate, times(1)).convertAndSend(
            "/game/room/" + 42,
            roomResponse
        );
    }
}