package apps.game.core.arena;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ArenaTest {
    private Arena arena;

    /**
     * reset for every test.
     */
    @BeforeEach
    public void setUp() {
        arena = new Arena(10, 10);
    }

    @Test
    public void testGetLastChar() {
        assertEquals('O', arena.getLastChar());

        arena.setGrid(0, 0, 'S');
        assertEquals('S', arena.getLastChar());

        arena.setGrid(0, 1, 'O');
        assertEquals('O', arena.getLastChar());
    }

    @Test
    public void testGetGrid() {
        arena.setGrid(0, 1, 'S');

        Grid[][] grid = arena.getGrid();

        assertEquals('S', grid[0][1].getStats());
    }

    @Test
    public void testGetGridWithXandY() {
        Grid grid = arena.getGrid(10, 10);

        assertNull(grid);

        arena.setGrid(9, 9, 'S');
        grid = arena.getGrid(9, 9);

        assertEquals('S', grid.getStats());
    }

    @Test
    public void testHorizontalCharSRight() {
        arena.setGrid(0, 2, 'S');
        arena.setGrid(0, 1, 'O');
        arena.setGrid(0, 0, 'S');

        Grid firstGrid = arena.getGrid(0, 0);
        assertTrue(firstGrid.getHorizontalStrike());

        Grid secondGrid = arena.getGrid(0, 1);
        assertTrue(secondGrid.getHorizontalStrike());

        Grid thirdGrid = arena.getGrid(0, 2);
        assertTrue(thirdGrid.getHorizontalStrike());
    }

    @Test
    public void testHorizontalCharSLeft() {
        arena.setGrid(0, 0, 'S');
        arena.setGrid(0, 1, 'O');
        arena.setGrid(0, 2, 'S');

        Grid thirdGrid = arena.getGrid(0, 2);
        assertTrue(thirdGrid.getHorizontalStrike());

        Grid secondGrid = arena.getGrid(0, 1);
        assertTrue(secondGrid.getHorizontalStrike());

        Grid firstGrid = arena.getGrid(0, 0);
        assertTrue(firstGrid.getHorizontalStrike());
    }

    @Test
    public void testHorizontalCharO() {
        arena.setGrid(0, 2, 'S');
        arena.setGrid(0, 0, 'S');
        arena.setGrid(0, 1, 'O');

        Grid firstGrid = arena.getGrid(0, 0);
        assertTrue(firstGrid.getHorizontalStrike());

        Grid secondGrid = arena.getGrid(0, 1);
        assertTrue(secondGrid.getHorizontalStrike());

        Grid thirdGrid = arena.getGrid(0, 2);
        assertTrue(thirdGrid.getHorizontalStrike());
    }

    @Test
    public void testVerticalCharSDown() {
        arena.setGrid(2, 0, 'S');
        arena.setGrid(1, 0, 'O');
        arena.setGrid(0, 0, 'S');

        Grid firstGrid = arena.getGrid(0, 0);
        assertTrue(firstGrid.getVerticalStrike());

        Grid secondGrid = arena.getGrid(1, 0);
        assertTrue(secondGrid.getVerticalStrike());

        Grid thirdGrid = arena.getGrid(2, 0);
        assertTrue(thirdGrid.getVerticalStrike());
    }

    @Test
    public void testVerticalCharSUp() {
        arena.setGrid(0, 0, 'S');
        arena.setGrid(1, 0, 'O');
        arena.setGrid(2, 0, 'S');

        Grid thirdGrid = arena.getGrid(2, 0);
        assertTrue(thirdGrid.getVerticalStrike());

        Grid secondGrid = arena.getGrid(1, 0);
        assertTrue(secondGrid.getVerticalStrike());

        Grid firstGrid = arena.getGrid(0, 0);
        assertTrue(firstGrid.getVerticalStrike());
    }

    @Test
    public void testVerticalCharO() {
        arena.setGrid(2, 0, 'S');
        arena.setGrid(0, 0, 'S');
        arena.setGrid(1, 0, 'O');

        Grid firstGrid = arena.getGrid(0, 0);
        assertTrue(firstGrid.getVerticalStrike());

        Grid secondGrid = arena.getGrid(1, 0);
        assertTrue(secondGrid.getVerticalStrike());

        Grid thirdGrid = arena.getGrid(2, 0);
        assertTrue(thirdGrid.getVerticalStrike());
    }

    @Test
    public void testRightDiagonalUp() {
        arena.setGrid(0, 2, 'S');
        arena.setGrid(1, 1, 'O');
        arena.setGrid(2, 0, 'S');

        Grid thirdGrid = arena.getGrid(2, 0);
        assertTrue(thirdGrid.getRightDiagonalStrike());

        Grid secondGrid = arena.getGrid(1, 1);
        assertTrue(secondGrid.getRightDiagonalStrike());

        Grid firstGrid = arena.getGrid(0, 2);
        assertTrue(firstGrid.getRightDiagonalStrike());
    }

    @Test
    public void testRightDiagonalDownCharS() {
        arena.setGrid(2, 2, 'S');
        arena.setGrid(1, 1, 'O');
        arena.setGrid(0, 0, 'S');

        Grid firstGrid = arena.getGrid(0, 0);
        assertTrue(firstGrid.getLeftDiagonalStrike());

        Grid secondGrid = arena.getGrid(1, 1);
        assertTrue(secondGrid.getLeftDiagonalStrike());

        Grid thirdGrid = arena.getGrid(2, 2);
        assertTrue(thirdGrid.getLeftDiagonalStrike());
    }

    @Test
    public void testRightDiagonalDownCharO() {
        arena.setGrid(2, 2, 'S');
        arena.setGrid(0, 0, 'S');
        arena.setGrid(1, 1, 'O');

        Grid firstGrid = arena.getGrid(0, 0);
        assertTrue(firstGrid.getLeftDiagonalStrike());

        Grid secondGrid = arena.getGrid(1, 1);
        assertTrue(secondGrid.getLeftDiagonalStrike());

        Grid thirdGrid = arena.getGrid(2, 2);
        assertTrue(thirdGrid.getLeftDiagonalStrike());
    }

    @Test
    public void testLeftDiagonalDownCharS() {
        arena.setGrid(2, 0, 'S');
        arena.setGrid(1, 1, 'O');
        arena.setGrid(0, 2, 'S');

        Grid firstGrid = arena.getGrid(0, 2);
        assertTrue(firstGrid.getRightDiagonalStrike());

        Grid secondGrid = arena.getGrid(1, 1);
        assertTrue(secondGrid.getRightDiagonalStrike());

        Grid thirdGrid = arena.getGrid(2, 0);
        assertTrue(thirdGrid.getRightDiagonalStrike());
    }

    @Test
    public void testLeftDiagonalDownCharO() {
        arena.setGrid(2, 0, 'S');
        arena.setGrid(0, 2, 'S');
        arena.setGrid(1, 1, 'O');

        Grid firstGrid = arena.getGrid(0, 2);
        assertTrue(firstGrid.getRightDiagonalStrike());

        Grid secondGrid = arena.getGrid(1, 1);
        assertTrue(secondGrid.getRightDiagonalStrike());

        Grid thirdGrid = arena.getGrid(2, 0);
        assertTrue(thirdGrid.getRightDiagonalStrike());
    }

    @Test
    public void testLeftDiagonalUp() {
        arena.setGrid(0, 0, 'S');
        arena.setGrid(1, 1, 'O');
        arena.setGrid(2, 2, 'S');

        Grid thirdGrid = arena.getGrid(2, 2);
        assertTrue(thirdGrid.getLeftDiagonalStrike());

        Grid secondGrid = arena.getGrid(1, 1);
        assertTrue(secondGrid.getLeftDiagonalStrike());

        Grid firstGrid = arena.getGrid(0, 0);
        assertTrue(firstGrid.getLeftDiagonalStrike());
    }

}