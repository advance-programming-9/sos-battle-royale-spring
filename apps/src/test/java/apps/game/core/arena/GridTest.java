package apps.game.core.arena;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class GridTest {
    private Grid grid;

    @BeforeEach
    public void setUp() {
        grid = new Grid();
    }

    @Test
    public void testSetGrid() {
        assertFalse(grid.getHasPressed());
        assertEquals('X', grid.getStats());

        grid.setGrid('S');

        assertTrue(grid.getHasPressed());
        assertEquals('S', grid.getStats());

        grid.setGrid('O');

        assertTrue(grid.getHasPressed());
        assertEquals('S', grid.getStats());
    }

    @Test
    public void testStrike() {
        assertFalse(grid.getHorizontalStrike());
        grid.strike(Strike.HORIZONTAL);
        assertTrue(grid.getHorizontalStrike());

        assertFalse(grid.getVerticalStrike());
        grid.strike(Strike.VERTICAL);
        assertTrue(grid.getVerticalStrike());

        assertFalse(grid.getLeftDiagonalStrike());
        grid.strike(Strike.LEFT_DIAGONAL);
        assertTrue(grid.getLeftDiagonalStrike());

        assertFalse(grid.getRightDiagonalStrike());
        grid.strike(Strike.RIGHT_DIAGONAL);
        assertTrue(grid.getRightDiagonalStrike());
    }
}