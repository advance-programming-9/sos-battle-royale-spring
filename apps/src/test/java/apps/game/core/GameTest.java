package apps.game.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import apps.user.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class GameTest {
    private Game game;

    /**
     * reset for every test.
     */
    @BeforeEach
    public void setUp() {
        game = new Game();
    }

    @Test
    public void testGetIsProtected() {
        assertFalse(game.getIsProtected());
    }

    @Test
    public void testRemoveUserAfterEnter() {
        User user = new User(123);

        boolean status = game.enter(user);

        assertTrue(status);
        assertEquals(1, game.getCountPeople());

        status = game.quit(user);

        assertTrue(status);
        assertEquals(0, game.getCountPeople());
    }

    @Test
    public void testRemoveUserBeforeEnter() {
        User user = new User(123);

        boolean status = game.quit(user);

        assertFalse(status);
    }

    @Test
    public void testFinishedGame() {
        User user = new User(123);
        User user2 = new User(124);

        game.addUser(user);
        game.addUser(user2);
        assertEquals("waiting", game.getState());
        boolean status = game.start();
        assertTrue(status);
        assertEquals("playing", game.getState());

        for (int x = 0; x < 10; x++) {
            for (int y = 0; y < 10; y++) {
                game.click(x, y, 'C');
            }
        }

        assertEquals("finished", game.getState());
    }

    @Test
    public void testCantStartWhenPlayingState() {
        User user = new User(123);

        game.addUser(user);
        assertEquals("waiting", game.getState());

        User user2 = new User(124);
        game.addUser(user2);

        boolean status = game.start();
        assertTrue(status);
        assertEquals("playing", game.getState());

        status = game.start();
        assertFalse(status);
        assertEquals("playing", game.getState());
    }

}