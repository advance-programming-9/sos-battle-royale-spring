package apps.game.core.state;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import apps.game.core.Game;
import apps.user.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PlayingStateTest {
    private GameState playingState;
    private Game game;

    /**
     * reset for every test.
     */
    @BeforeEach
    public void setUp() {
        game = new Game();
        playingState = new PlayingState(game);
    }

    @Test
    public void testEnter() {
        User user = new User(123);

        boolean status = playingState.enter(user);

        assertFalse(status);
    }

    @Test
    public void testEnterAfterFinishedAlreadyEnter() {
        game = new Game();
        User user = new User(123L);
        user.setUsername("fulan");
        game.enter(user);
        playingState = new PlayingState(game);
        boolean status = playingState.enter(user);

        assertTrue(status);
    }

    @Test
    public void testStart() {
        boolean status = playingState.start();

        assertFalse(status);
    }

    @Test
    public void testFinish() {
        boolean status = playingState.finish();

        assertTrue(status);
    }

    @Test
    public void testQuit() {
        User user = new User(123);

        boolean status = playingState.quit(user);

        assertFalse(status);
    }

    @Test
    public void testCanClickWhenPlaying() {
        User user = new User(123);
        game.addUser(user);

        playingState.click(1, 1, 'S');
        playingState.click(1, 2, 'O');
        long score = playingState.click(1, 3, 'S');

        assertEquals(1, score);
    }

    @Test
    public void testToString() {
        assertEquals("playing", playingState.toString());
    }
}