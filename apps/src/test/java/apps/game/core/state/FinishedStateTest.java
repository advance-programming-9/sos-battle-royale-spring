package apps.game.core.state;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import apps.game.core.Game;
import apps.user.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class FinishedStateTest {
    private GameState finishedState;
    private Game game;

    /**
     * reset for every test.
     */
    @BeforeEach
    public void setUp() {
        game = new Game();
        finishedState = new FinishedState(game);
    }

    @Test
    public void testEnter() {
        User user = new User(123);

        boolean status = finishedState.enter(user);

        assertFalse(status);
    }

    @Test
    public void testEnterAfterFinishedAlreadyEnter() {
        game = new Game();
        User user = new User(123L);
        user.setUsername("fulan");
        game.enter(user);
        finishedState = new FinishedState(game);
        boolean status = finishedState.enter(user);

        assertTrue(status);
    }

    @Test
    public void testStart() {
        boolean status = finishedState.start();

        assertFalse(status);
    }

    @Test
    public void testFinish() {
        boolean status = finishedState.finish();

        assertFalse(status);
    }

    @Test
    public void testQuit() {
        User user = new User(123);
        game.addUser(user);

        boolean status = finishedState.quit(user);

        assertTrue(status);
    }

    @Test
    public void testCantClickWhenFinished() {
        long score = finishedState.click(1, 1, 'C');

        assertEquals(0, score);
    }

    @Test
    public void testToString() {
        assertEquals("finished", finishedState.toString());
    }
}