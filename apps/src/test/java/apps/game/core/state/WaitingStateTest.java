package apps.game.core.state;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import apps.game.core.Game;
import apps.user.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class WaitingStateTest {
    private GameState waitingState;
    private Game game;

    /**
     * reset for every test.
     */
    @BeforeEach
    public void setUp() {
        game = new Game();
        User user = new User(123);
        game.enter(user);
        waitingState = new WaitingState(game);
    }

    @Test
    public void testEnter() {
        User user = new User(123);

        boolean status = waitingState.enter(user);

        assertTrue(status);
    }

    @Test
    public void testStart() {
        User user2 = new User(124);
        game.enter(user2);
        boolean status = waitingState.start();

        assertTrue(status);
    }

    @Test
    public void testFinish() {
        boolean status = waitingState.finish();

        assertFalse(status);
    }

    @Test
    public void testQuit() {
        User user = new User(123);
        waitingState.enter(user);

        boolean status = waitingState.quit(user);

        assertTrue(status);
    }

    @Test
    public void testCantClickWhenWaiting() {
        long score = waitingState.click(1, 1, 'C');

        assertEquals(0, score);
    }

    @Test
    public void testToString() {
        assertEquals("waiting", waitingState.toString());
    }
}