package apps.scoreboard.controller;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import apps.scoreboard.core.Scoreboard;
import apps.scoreboard.core.adapter.SafeUser;
import apps.user.auth.JWT;
import apps.user.model.User;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@WebMvcTest(controllers = ScoreboardController.class)
public class ScoreboardControllerTest {
    private final Gson gson = new Gson();

    Logger logger = LoggerFactory.getLogger(ScoreboardControllerTest.class);

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private JWT jwt;

    @MockBean
    private Scoreboard scoreboard;

    @Test
    public void testFetchScoreboardInvalidAuthorizationToken() throws Exception {
        mockMvc.perform(get("/scoreboard/fetch").header("authorization", "Bearer asd"))
            .andExpect(status().isBadRequest())
            .andExpect(handler().methodName("fetchScoreboard"));
    }

    @Test
    public void testFetchScoreboardValid() throws Exception {
        List<User> users = new ArrayList<>();

        User user1 = new User(1);
        user1.setUsername("fulan1");
        user1.setScore(100);
        users.add(user1);

        User user2 = new User(2);
        user2.setUsername("fulan2");
        user2.setScore(101);
        users.add(user2);

        User user3 = new User(3);
        user3.setUsername("fulan3");
        user3.setScore(102);
        users.add(user3);

        String token = "asd";

        when(jwt.validateToken(token)).thenReturn(true);
        when(scoreboard.getTopTen()).thenReturn(users);

        MvcResult response = mockMvc.perform(get("/scoreboard/fetch").header(
            "authorization", "Bearer " + token)
        )
            .andExpect(status().isOk())
            .andExpect(handler().methodName("fetchScoreboard")).andReturn();

        String jsonResponse = response.getResponse().getContentAsString();

        SafeUser[] safeUsers = gson.fromJson(jsonResponse, SafeUser[].class);

        assertNotNull(safeUsers[0]);
        assertNotNull(safeUsers[1]);
        assertNotNull(safeUsers[2]);
        assertNull(safeUsers[3]);
        assertNull(safeUsers[4]);
        assertNull(safeUsers[5]);
        assertNull(safeUsers[6]);
        assertNull(safeUsers[7]);
        assertNull(safeUsers[8]);
        assertNull(safeUsers[9]);
    }

}