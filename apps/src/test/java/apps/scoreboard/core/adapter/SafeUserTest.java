package apps.scoreboard.core.adapter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import apps.user.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCrypt;

public class SafeUserTest {
    private User user;
    private SafeUser safeUser;
    private String username = "fulan";
    private String email = "fulan@fulan.id";
    private String password = "dummy_password";
    private long score = 200L;

    /**
     * setup new user.
     */
    @BeforeEach
    public void setUp() {
        user = new User();
        user.setUsername(username);
        user.setEmail(email);
        user.setPassword(BCrypt.hashpw(password, BCrypt.gensalt(4)));
        user.setScore(score);
        safeUser = new SafeUser(user);
    }

    @Test
    public void testUserGetScore() {
        assertEquals(200,safeUser.getScore());
    }

    @Test
    public void testGetUsername() {
        assertEquals("fulan",safeUser.getUsername());
    }

    @Test
    public void testGetInGameScore() {
        assertEquals(0,safeUser.getInGameScore());
    }

    @Test
    public void testSetInGameScore() {
        safeUser.setInGameScore(10);

        assertEquals(10,safeUser.getInGameScore());
    }

    @Test
    public void testCompareToWithDummyInGameScoreMoreThanPlayer() {
        User user2 = new User();
        user2.setUsername("fulan2");
        user2.setEmail("fulan2@gmail.com");
        user2.setPassword(BCrypt.hashpw("fulan2GG123", BCrypt.gensalt(4)));
        user2.setScore(200);
        SafeUser safeUser2 = new SafeUser(user2);
        safeUser2.setInGameScore(100);

        assertTrue(safeUser2.compareTo(safeUser) == -1);
    }

    @Test
    public void testCompareToWithDummyInGameScoreLowerThanPlayer() {
        User user2 = new User();
        user2.setUsername("fulan2");
        user2.setEmail("fulan2@gmail.com");
        user2.setPassword(BCrypt.hashpw("fulan2GG123", BCrypt.gensalt(4)));
        user2.setScore(200);
        SafeUser safeUser2 = new SafeUser(user2);
        safeUser2.setInGameScore(0);
        safeUser.setInGameScore(10);

        assertTrue(safeUser2.compareTo(safeUser) == 1);
    }

    @Test
    public void testCompareToWithDummyInGameScoreEqualThanPlayer() {
        User user2 = new User();
        user2.setUsername("fulan2");
        user2.setEmail("fulan2@gmail.com");
        user2.setPassword(BCrypt.hashpw("fulan2GG123", BCrypt.gensalt(4)));
        user2.setScore(200);
        SafeUser safeUser2 = new SafeUser(user2);
        safeUser2.setInGameScore(10);
        safeUser.setInGameScore(10);

        assertTrue(safeUser2.compareTo(safeUser) == 0);
    }
}
