package apps.scoreboard.core;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import apps.user.model.User;
import apps.user.repository.UserRepositoryJPA;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.bcrypt.BCrypt;

@ExtendWith(MockitoExtension.class)
public class ScoreboardTest {

    @Mock
    private UserRepositoryJPA userRepositoryJPA;

    @InjectMocks
    private Scoreboard scoreboard;

    @Test
    public void testUpdate() {
        User user = new User();
        user.setUsername("fulan2");
        user.setEmail("fulan2@gmail.com");
        user.setPassword(BCrypt.hashpw("fulanGG123", BCrypt.gensalt(4)));
        user.setScore(100L);
        scoreboard.update(user);

        verify(userRepositoryJPA, times(1)).updateScoreById(user.getId(),100L);
    }

    @Test
    public void testGetTopTen() {
        scoreboard.getTopTen();

        verify(userRepositoryJPA,times(1)).findByOrderByScoreDescLimit10();
    }
}
