package apps.user.model.validator;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import apps.user.model.validator.atomic.EmailValidator;
import apps.user.model.validator.atomic.PasswordValidator;
import apps.user.model.validator.atomic.UsernameValidator;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class AtomicTest {

    /**
     * test constructors.
     */
    @BeforeAll
    public static void setUp() {
        new UsernameValidator();
        new EmailValidator();
        new PasswordValidator();
    }

    @Test
    public void testUsernameValidatorLessThanThreeOrMoreThanFifteenIsNotValid() {
        assertFalse(UsernameValidator.validate("ab"));
        assertFalse(UsernameValidator.validate("asdasdasdasdasda"));
    }

    @Test
    public void testUsernameValidatorAlphanumeric() {
        assertFalse(UsernameValidator.validate("abaa+"));
        assertTrue(UsernameValidator.validate("abaa"));
    }

    @Test
    public void testEmailValidatorLessThanThreeOrMoreThan100IsNotValid() {
        assertFalse(EmailValidator.validate("ab"));
        assertFalse(EmailValidator.validate(
            "as@" + new String(new char[95]).replace("\0", "a") + ".aa")
        );
    }

    @Test
    public void testEmailValidatorPattern() {
        assertTrue(EmailValidator.validate("fulan@fulan.id"));
        assertTrue(EmailValidator.validate("+@a.id"));
        assertTrue(EmailValidator.validate("_@a.id"));

        assertFalse(EmailValidator.validate("fulan@fulan.i"));
        assertFalse(EmailValidator.validate("fulan@.id"));
        assertFalse(EmailValidator.validate("fulan@+.id"));
        assertFalse(EmailValidator.validate("@a.id"));
        assertFalse(EmailValidator.validate("+@a.i+"));
        assertFalse(EmailValidator.validate("(@a.id"));
        assertFalse(EmailValidator.validate(")@a.id"));
        assertFalse(EmailValidator.validate("@@a.id"));
        assertFalse(EmailValidator.validate("#@a.id"));
        assertFalse(EmailValidator.validate("$@a.id"));
        assertFalse(EmailValidator.validate("%@a.id"));
        assertFalse(EmailValidator.validate("^@a.id"));
        assertFalse(EmailValidator.validate("&@a.id"));
        assertFalse(EmailValidator.validate("*@a.id"));
        assertFalse(EmailValidator.validate("=@a.id"));
        assertFalse(EmailValidator.validate("`@a.id"));
        assertFalse(EmailValidator.validate("~@a.id"));

        // etc
    }

    @Test
    public void testPasswordValidatorLessThanThreeOrMoreThan100IsNotValid() {
        assertTrue(PasswordValidator.validate("abc"));
        assertTrue(PasswordValidator.validate(new String(new char[100]).replace("\0", "a")));

        assertFalse(PasswordValidator.validate("ab"));
        assertFalse(PasswordValidator.validate(new String(new char[101]).replace("\0", "a")));
    }
}