package apps.user.model.validator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import apps.user.model.User;
import apps.user.repository.UserRepositoryJPA;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCrypt;

@ExtendWith(MockitoExtension.class)
public class UserRegiterWithValidatorTest {

    Logger logger = LoggerFactory.getLogger(UserRegiterWithValidatorTest.class);

    @InjectMocks
    private UserRegisterWithValidator userRegisterWithValidator;

    @Mock
    private UserRepositoryJPA userRepositoryJPA;

    @BeforeEach
    public void setUp() {
        UserRegisterWithValidator.setRepository(userRepositoryJPA);
    }

    @Test
    public void testSetUsernameValid() {
        String username = "fulan";
        userRegisterWithValidator.setUsername(username);
        
        verify(userRepositoryJPA, times(1)).findByUsername(username);
        
        assertEquals(
            null,
            userRegisterWithValidator.getUsernameError()
        );
        assertTrue(userRegisterWithValidator.getIsValid());

        User user = userRegisterWithValidator.getUser();

        assertNotEquals(null, user);

        String actualUsername = user.getUsername();

        assertEquals(username, actualUsername);
    }

    @Test
    public void testSetUsernameLessThanThreeIsNotValid() {
        userRegisterWithValidator.setUsername("as");
        
        verify(userRepositoryJPA, times(0)).findByUsername("as");

        assertEquals(
            "Invalid username, make sure the length is at least 3 and only contains alphanumeric",
            userRegisterWithValidator.getUsernameError()
        );
        assertFalse(userRegisterWithValidator.getIsValid());
        assertEquals(null, userRegisterWithValidator.getUser());
    }

    @Test
    public void testSetUsernameMoreThanFifteenIsNotValid() {
        userRegisterWithValidator.setUsername("asdasdasdasdasda");
        
        verify(userRepositoryJPA, times(0)).findByUsername("asdasdasdasdasda");

        assertEquals(
            "Invalid username, make sure the length is at least 3 and only contains alphanumeric",
            userRegisterWithValidator.getUsernameError()
        );
        assertFalse(userRegisterWithValidator.getIsValid());
        assertEquals(null, userRegisterWithValidator.getUser());
    }

    @Test
    public void testSetUsernameNotAlphanumeric() {
        userRegisterWithValidator.setUsername("as+");
        
        verify(userRepositoryJPA, times(0)).findByUsername("as+");

        assertEquals(
            "Invalid username, make sure the length is at least 3 and only contains alphanumeric",
            userRegisterWithValidator.getUsernameError()
        );
        assertFalse(userRegisterWithValidator.getIsValid());
        assertEquals(null, userRegisterWithValidator.getUser());
    }

    @Test
    public void testSetUsernameIsPresent() {
        Optional<User> optional = Optional.of(new User());
        when(userRepositoryJPA.findByUsername("fulan")).thenReturn(optional);

        userRegisterWithValidator.setUsername("fulan");
        
        verify(userRepositoryJPA, times(1)).findByUsername("fulan");

        assertEquals(
            "This username already exists, please choose another username",
            userRegisterWithValidator.getUsernameError()
        );
        assertFalse(userRegisterWithValidator.getIsValid());
        assertEquals(null, userRegisterWithValidator.getUser());
    }

    @Test
    public void testSetEmailValid() {
        String email = "fulan@fulan.id";
        userRegisterWithValidator.setEmail(email);

        verify(userRepositoryJPA, times(1)).findByEmail(email);

        assertEquals(
            null,
            userRegisterWithValidator.getEmailError()
        );
        assertTrue(userRegisterWithValidator.getIsValid());
        assertNotEquals(null, userRegisterWithValidator.getUser());

        User user = userRegisterWithValidator.getUser();
        String actualEmail = user.getEmail();

        assertEquals(email, actualEmail);
    }

    @Test
    public void testSetEmailLessThanThreeIsNotValid() {
        userRegisterWithValidator.setEmail("fu");
        
        verify(userRepositoryJPA, times(0)).findByEmail("fu");

        assertEquals(
            "Invalid email, please enter a valid email",
            userRegisterWithValidator.getEmailError()
        );
        assertFalse(userRegisterWithValidator.getIsValid());
        assertEquals(null, userRegisterWithValidator.getUser());
    }

    @Test
    public void testSetEmailMoreThan100IsNotValid() {
        String email = new String(new char[100]).replace("\0", "a");
        userRegisterWithValidator.setEmail(email);
        
        verify(userRepositoryJPA, times(0)).findByEmail(email);

        assertEquals(
            "Invalid email, please enter a valid email",
            userRegisterWithValidator.getEmailError()
        );
        assertFalse(userRegisterWithValidator.getIsValid());
        assertEquals(null, userRegisterWithValidator.getUser());
    }

    @Test
    public void testSetEmailNotValidPattern() {
        userRegisterWithValidator.setEmail("fulan@");
        
        verify(userRepositoryJPA, times(0)).findByEmail("fulan@");

        assertEquals(
            "Invalid email, please enter a valid email",
            userRegisterWithValidator.getEmailError()
        );
        assertFalse(userRegisterWithValidator.getIsValid());
        assertEquals(null, userRegisterWithValidator.getUser());
    }

    @Test
    public void testSetEmailIsPresent() {
        Optional<User> optional = Optional.of(new User());
        when(userRepositoryJPA.findByEmail("fulan@fulan.id")).thenReturn(optional);

        userRegisterWithValidator.setEmail("fulan@fulan.id");
        
        verify(userRepositoryJPA, times(1)).findByEmail("fulan@fulan.id");

        assertEquals(
            "This email already exists, please choose another email",
            userRegisterWithValidator.getEmailError()
        );
        assertFalse(userRegisterWithValidator.getIsValid());
        assertEquals(null, userRegisterWithValidator.getUser());
    }

    @Test
    public void testSetPasswordValid() {
        userRegisterWithValidator.setPassword("dummy_password");
        
        User user = userRegisterWithValidator.getUser();

        assertNotEquals(null, user);
        assertTrue(
            BCrypt.checkpw("dummy_password", user.getPassword())
        );
        assertTrue(userRegisterWithValidator.getIsValid());
    }

    @Test
    public void testSetPasswordLessThanThreeIsNotValid() {
        userRegisterWithValidator.setPassword("du");
        User user = userRegisterWithValidator.getUser();
        
        assertEquals(
            "Invalid password, make sure it consists of at least 3 characters "
            + "and max 100 characters", 
            userRegisterWithValidator.getPasswordError()
        );
        assertFalse(userRegisterWithValidator.getIsValid());
        assertEquals(null, user);
    }

    @Test
    public void testSetPasswordMoreThan100IsNotValid() {
        String password = new String(new char[101]).replace("\0", "a");
        userRegisterWithValidator.setPassword(password);
        User user = userRegisterWithValidator.getUser();
        
        assertEquals(
            "Invalid password, make sure it consists of at least 3 characters "
            + "and max 100 characters",
            userRegisterWithValidator.getPasswordError()
        );
        assertFalse(userRegisterWithValidator.getIsValid());
        assertEquals(null, user);
    }

    @Test
    public void testSetSecondPasswordIsNotEqualFirstPassword() {
        String password1 = "asdasd";
        String password2 = "asdasdd";

        userRegisterWithValidator.setPassword(password1);
        userRegisterWithValidator.setPassword2(password2);

        User user = userRegisterWithValidator.getUser();
        
        assertEquals(
            "Invalid password, make sure you repeat the same password", 
            userRegisterWithValidator.getPasswordError()
        );
        assertFalse(userRegisterWithValidator.getIsValid());
        assertEquals(null, user);
    }

    @Test
    public void testSetSecondPasswordIsEqualFirstPassword() {
        String password1 = "asdasd";
        String password2 = "asdasd";

        userRegisterWithValidator.setPassword(password1);
        userRegisterWithValidator.setPassword2(password2);

        User user = userRegisterWithValidator.getUser();
        
        assertNotEquals(null, user);
        assertEquals(
            null, 
            userRegisterWithValidator.getPasswordError()
        );
        assertTrue(userRegisterWithValidator.getIsValid());
    }
}