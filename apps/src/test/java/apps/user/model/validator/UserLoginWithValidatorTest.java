package apps.user.model.validator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import apps.user.model.User;
import apps.user.repository.UserRepositoryJPA;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCrypt;

@ExtendWith(MockitoExtension.class)
public class UserLoginWithValidatorTest {

    Logger logger = LoggerFactory.getLogger(UserLoginWithValidator.class);

    @InjectMocks
    private UserLoginWithValidator userLoginWithValidator;

    @Mock
    private UserRepositoryJPA userRepositoryJPA;

    @BeforeEach
    public void setUp() {
        UserLoginWithValidator.setRepository(userRepositoryJPA);
    }

    @Test
    public void testSetUsernameValid() {
        String username = "fulan";

        userLoginWithValidator.setUsername(username);

        assertTrue(userLoginWithValidator.getIsValid());
        assertNull(userLoginWithValidator.getUsernameError());
    }

    @Test
    public void testSetUsernameLessThanThreeIsNotValid() {
        String username = "fu";

        userLoginWithValidator.setUsername(username);

        assertFalse(userLoginWithValidator.getIsValid());
        assertEquals(
            "Invalid username, make sure the length is at least 3 and only contains alphanumeric", 
            userLoginWithValidator.getUsernameError()
        );
        assertNull(userLoginWithValidator.getUser());
    }

    @Test
    public void testSetUsernameMoreThanFifteenIsNotValid() {
        String username = "asdasdasdasdasda";

        userLoginWithValidator.setUsername(username);

        assertFalse(userLoginWithValidator.getIsValid());
        assertEquals(
            "Invalid username, make sure the length is at least 3 and only contains alphanumeric", 
            userLoginWithValidator.getUsernameError()
        );
        assertNull(userLoginWithValidator.getUser());
    }

    @Test
    public void testSetPasswordValid() {
        String password = "dummy";

        userLoginWithValidator.setPassword(password);

        assertTrue(userLoginWithValidator.getIsValid());
        assertNull(userLoginWithValidator.getUsernameError());
    }

    @Test
    public void testSetPasswordLessThanThreeIsNotValid() {
        String password = "fu";

        userLoginWithValidator.setPassword(password);

        assertFalse(userLoginWithValidator.getIsValid());
        assertEquals(
            "Invalid password, make sure it consists of at least 3 characters", 
            userLoginWithValidator.getPasswordError()
        );
    }

    @Test
    public void testSetPasswordMoreThan100IsNotValid() {
        String password = new String(new char[101]).replace("\0", "a");

        userLoginWithValidator.setPassword(password);

        assertFalse(userLoginWithValidator.getIsValid());
        assertEquals(
            "Invalid password, make sure it consists of at least 3 characters", 
            userLoginWithValidator.getPasswordError()
        );
    }

    @Test
    public void testValidLogin() {
        User user = new User();
        user.setUsername("fulan");
        user.setEmail("fulan@fulan.id");
        user.setPassword(BCrypt.hashpw("dummy", BCrypt.gensalt(4)));
        user.setScore(200L);

        when(userRepositoryJPA.findByUsername("fulan")).thenReturn(Optional.of(user));

        userLoginWithValidator.setUsername("fulan");
        userLoginWithValidator.setPassword("dummy");

        assertTrue(userLoginWithValidator.getIsValid());
        assertNull(userLoginWithValidator.getUsernameError());
        assertNull(userLoginWithValidator.getPasswordError());

        User actualUser = userLoginWithValidator.getUser();

        assertEquals(user, actualUser);
    }

    @Test
    public void testLoginUsernameNotFound() {
        when(userRepositoryJPA.findByUsername("fulan")).thenReturn(Optional.empty());

        userLoginWithValidator.setUsername("fulan");
        userLoginWithValidator.setPassword("dummy");

        assertFalse(userLoginWithValidator.getIsValid());
        assertEquals("Incorrect username or password", userLoginWithValidator.getInvalidLogin());

        User actualUser = userLoginWithValidator.getUser();

        assertNull(actualUser);
    }

    @Test
    public void testLoginInvalidPassword() {
        User user = new User();
        user.setUsername("fulan");
        user.setEmail("fulan@fulan.id");
        user.setPassword(BCrypt.hashpw("dummy", BCrypt.gensalt(4)));
        user.setScore(200L);

        when(userRepositoryJPA.findByUsername("fulan")).thenReturn(Optional.of(user));

        userLoginWithValidator.setUsername("fulan");
        userLoginWithValidator.setPassword("dummyy");

        assertFalse(userLoginWithValidator.getIsValid());
        assertEquals("Incorrect username or password", userLoginWithValidator.getInvalidLogin());

        User actualUser = userLoginWithValidator.getUser();

        assertNotEquals(user, actualUser);
    }

    @Test
    public void testSetJwtToken() {
        userLoginWithValidator.setJwtToken("eyeyey");

        assertEquals("eyeyey", userLoginWithValidator.getJwtToken());
    }
}