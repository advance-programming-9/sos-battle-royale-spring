package apps.user.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCrypt;

public class UserTest {
    private User user;
    private String username = "fulan";
    private String email = "fulan@fulan.id";
    private String password = "dummy_password";
    private long score = 200L;
    private long id = 100L;

    /**
     * setup new user.
     */
    @BeforeEach
    public void setUp() {
        user = new User(id);
        user.setUsername(username);
        user.setEmail(email);
        user.setPassword(BCrypt.hashpw(password, BCrypt.gensalt(4)));
        user.setScore(score);
    }

    @Test
    public void testId() {
        long actualId = user.getId();

        assertEquals(id, actualId);
    }

    @Test
    public void testUsername() {
        String actualUsername = user.getUsername();

        assertEquals(username, actualUsername);
    }

    @Test
    public void testEmail() {
        String actualEmail = user.getEmail();

        assertEquals(email, actualEmail);
    }

    @Test
    public void testPassword() {
        String actualPassword = user.getPassword();

        assertTrue(
            BCrypt.checkpw(password, actualPassword)
        );
    }

    @Test
    public void testScore() {
        long actualScore = user.getScore();

        assertEquals(score, actualScore);
    }

    @Test
    public void testAddFriend() {
        User friendUser = new User();
        friendUser.setUsername("fulan2");
        friendUser.setEmail("fulan2@gmail.com");
        friendUser.setPassword(BCrypt.hashpw("fulanGG123", BCrypt.gensalt(4)));
        friendUser.setScore(100L);
        user.addFriend(friendUser);
        Set<User> contentOfFriends = new HashSet<>();
        contentOfFriends.add(friendUser);

        assertEquals(contentOfFriends,user.getFriends());
    }

    @Test
    public void testDeleteFriend() {
        User friendUser = new User();
        friendUser.setUsername("fulan2");
        friendUser.setEmail("fulan2@gmail.com");
        friendUser.setPassword(BCrypt.hashpw("fulanGG123", BCrypt.gensalt(4)));
        friendUser.setScore(100L);
        user.addFriend(friendUser);
        Set<User> contentOfFriends = new HashSet<>();
        contentOfFriends.add(friendUser);

        assertEquals(contentOfFriends,user.getFriends());
        user.deleteFriend(friendUser);
        contentOfFriends.remove(friendUser);

        assertEquals(contentOfFriends,user.getFriends());
    }
}