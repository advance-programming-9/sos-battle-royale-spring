package apps.user.auth;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import java.security.Key;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

public class JWTTest {

    private String secretKey = "dummy_secret_for_testingdummy_secret_for_testingdummy_secret_for";

    // 60 minutes
    private static final long JWT_TOKEN_VALIDITY = 60 * 60 * 1000;

    @InjectMocks
    private JWT jwt = new JWT(secretKey);

    @Test
    public void testCreateTokenAndGetUsernameShouldBeSame() {
        String username = "fulan";

        String token = jwt.createToken(username);

        String username2 = jwt.getUsernameFromToken(token);
        assertEquals(username, username2);
    }

    @Test
    public void testGetExipartionDateShouldBeOneHour() {
        String token = jwt.createToken("fulan");

        Date now = new Date();
        Date oneHour = new Date(now.getTime() + JWT_TOKEN_VALIDITY);

        Date expirationDate = jwt.getExpirationDateFromToken(token);

        long diff = Math.abs(expirationDate.getTime() - oneHour.getTime());
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;

        assertEquals(0L, diffMinutes);
        assertEquals(0L, diffHours);
    }

    @Test
    public void testRefreshTokenShouldGenerateNewToken() throws InterruptedException {
        String token = jwt.createToken("fulan");
        TimeUnit.SECONDS.sleep(1);
        String refreshedToken = jwt.refreshToken(token);

        assertNotEquals(token, refreshedToken);
    }

    @Test
    public void testValidateToken() {
        String username = "fulan";
        String token = jwt.createToken(username);

        assertTrue(jwt.validateToken(token));

        Claims claims = Jwts.claims().setSubject(username);
        Date now = new Date();
        Date validity = new Date(now.getTime() - 60 * 1000);
        Key key = Keys.hmacShaKeyFor(secretKey.getBytes());

        
        String customToken = Jwts.builder()
            .setClaims(claims)
            .setIssuedAt(now)
            .setExpiration(validity)
            .signWith(key)
            .compact();

        assertFalse(jwt.validateToken(customToken));
    }
}