package apps.user.auth;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import apps.user.response.JwtResponse;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import java.security.Key;
import java.util.Date;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

public class JwtAuthorizationTest {

    private String secretKey = "dummy_secret_for_testingdummy_secret_for_testingdummy_secret_for";

    @Mock
    private JWT jwt = new JWT(secretKey);

    @InjectMocks
    private JwtAuthorization jwtAuthorization = new JwtAuthorization(jwt);

    @Test
    public void testSetAuthorizationValid() {
        String username = "fulan";
        String token = jwt.createToken(username);

        jwtAuthorization.setAuthorization("Bearer " + token);

        assertEquals(username, jwtAuthorization.getUsername());
        assertNull(jwtAuthorization.getError());
        assertEquals(token, jwtAuthorization.getJwtToken());
        assertEquals("Bearer " + token, jwtAuthorization.getAuthorization());

        JwtResponse jwtResponse = jwtAuthorization.getJwtResponse();

        assertNotNull(jwtResponse);
        assertTrue(jwtResponse.getIsValidated());
        assertNotNull(jwtResponse.getRefreshedToken());
    }

    @Test
    public void testSetAuthorizationLengthIsNot2Invalid() {
        String auth = "test";

        jwtAuthorization.setAuthorization(auth);

        assertEquals("INVALID_JWT", jwtAuthorization.getError());

        String usernameInvalid = jwtAuthorization.getUsername();

        assertNull(usernameInvalid);

        JwtResponse jwtResponse = jwtAuthorization.getJwtResponse();

        assertNull(jwtResponse);
    }

    @Test
    public void testSetAuthorizationNotBearerInvalid() {
        String auth = "asd test";

        jwtAuthorization.setAuthorization(auth);

        assertEquals("INVALID_JWT", jwtAuthorization.getError());

        String usernameInvalid = jwtAuthorization.getUsername();

        assertNull(usernameInvalid);

        JwtResponse jwtResponse = jwtAuthorization.getJwtResponse();

        assertNull(jwtResponse);
    }

    @Test
    public void testSetAuthorizationInvalidSignature() {
        String auth = "Bearer " + "eyey";

        jwtAuthorization.setAuthorization(auth);

        assertEquals("INVALID_JWT", jwtAuthorization.getError());

        String usernameInvalid = jwtAuthorization.getUsername();

        assertNull(usernameInvalid);

        JwtResponse jwtResponse = jwtAuthorization.getJwtResponse();

        assertNull(jwtResponse);
    }

    @Test
    public void testSetAuthorizationExpiredJwtInvalid() {
        String username = "fulan";

        Claims claims = Jwts.claims().setSubject(username);
        Date now = new Date();
        Date validity = new Date(now.getTime() - 60 * 1000);
        Key key = Keys.hmacShaKeyFor(secretKey.getBytes());

        String customToken = Jwts.builder()
            .setClaims(claims)
            .setIssuedAt(now)
            .setExpiration(validity)
            .signWith(key)
            .compact();
        
        jwtAuthorization.setAuthorization("Bearer " + customToken);
        
        assertEquals("INVALID_JWT", jwtAuthorization.getError());

        String usernameInvalid = jwtAuthorization.getUsername();

        assertNull(usernameInvalid);

        JwtResponse jwtResponse = jwtAuthorization.getJwtResponse();

        assertNull(jwtResponse);
    }
}