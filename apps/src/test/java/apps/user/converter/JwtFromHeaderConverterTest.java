package apps.user.converter;

import static org.junit.jupiter.api.Assertions.assertNull;

import apps.user.auth.JWT;
import apps.user.auth.JwtAuthorization;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

public class JwtFromHeaderConverterTest {

    private String secretKey = "dummy_secret_for_testingdummy_secret_for_testingdummy_secret_for";

    @Mock
    private JWT jwt = new JWT(secretKey);

    @InjectMocks
    private JwtFromHeaderConverter jwtFromHeaderConverter = new JwtFromHeaderConverter(jwt);

    @Test
    public void testConvert() {
        String token = jwt.createToken("fulan");
        JwtAuthorization jwtAuthorization = jwtFromHeaderConverter.convert("Bearer " + token);

        assertNull(jwtAuthorization.getError());
    }
}