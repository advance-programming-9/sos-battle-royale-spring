package apps.user.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import apps.user.auth.JWT;
import apps.user.model.User;
import apps.user.model.validator.UserLoginWithValidator;
import apps.user.model.validator.UserRegisterWithValidator;
import apps.user.repository.MatchRepository;
import apps.user.repository.UserRepositoryJPA;
import apps.user.response.FetchMatchResponse;
import apps.user.response.JwtResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;


@WebMvcTest(controllers = UserController.class)
public class UserControllerTest {
    private final Gson gson = new Gson();

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MatchRepository matchRepository;

    @MockBean
    private UserRepositoryJPA userRepositoryJPA;

    @MockBean
    private JWT jwt;

    @Test
    public void testRegister() throws Exception {
        Map<String,String> apa = new HashMap<>();
        apa.put("username", "fulan");
        apa.put("email", "fulan@fulan.id");
        apa.put("password", "dummy_password");
        apa.put("password2", "dummy_password");

        String json = new ObjectMapper().writeValueAsString(apa);

        MvcResult response = mockMvc.perform(
            post("/user/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(handler().methodName("register")).andReturn();

        String jsonResponse = response.getResponse().getContentAsString();

        UserRegisterWithValidator userRegisterWithValidator = gson.fromJson(
            jsonResponse,
            UserRegisterWithValidator.class
        );

        assertEquals(null, userRegisterWithValidator.getUsernameError());
        assertEquals(null, userRegisterWithValidator.getEmailError());
        assertEquals(null, userRegisterWithValidator.getPasswordError());
        assertTrue(userRegisterWithValidator.getIsValid());
    }

    @Test
    public void testRegisterInvalid() throws Exception {
        Map<String,String> apa = new HashMap<>();
        apa.put("username", "fu");
        apa.put("email", "fulan@.id");
        apa.put("password", "du");
        apa.put("password2", "du");

        String json = new ObjectMapper().writeValueAsString(apa);

        MvcResult response = mockMvc.perform(
            post("/user/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest())
            .andExpect(handler().methodName("register")).andReturn();

        String jsonResponse = response.getResponse().getContentAsString();

        UserRegisterWithValidator validated = gson.fromJson(
            jsonResponse,
            UserRegisterWithValidator.class
        );

        assertEquals(
            "Invalid username, make sure the length is at least 3 "
            + "and only contains alphanumeric",
            validated.getUsernameError()
        );
        assertEquals(
            "Invalid email, please enter a valid email",
            validated.getEmailError()
        );
        assertEquals(
            "Invalid password, make sure it consists of at least 3 characters"
            + " and max 100 characters",
            validated.getPasswordError()
        );
        assertFalse(validated.getIsValid());
    }

    @Test
    public void testCanLoginAfterRegister() throws Exception {
        Map<String,String> apa = new HashMap<>();
        apa.put("username", "fulan");
        apa.put("email", "fulan@fulan.id");
        apa.put("password", "dummy_password");
        apa.put("password2", "dummy_password");

        String json = new ObjectMapper().writeValueAsString(apa);

        mockMvc.perform(
            post("/user/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(handler().methodName("register"));

        apa = new HashMap<>();
        apa.put("username", "fulan");
        apa.put("password", "dummy_password");

        User user = new User();
        user.setUsername("fulan");
        user.setEmail("fulan@fulan.id");
        user.setPassword(BCrypt.hashpw("dummy_password", BCrypt.gensalt(4)));

        when(userRepositoryJPA.findByUsername("fulan")).thenReturn(Optional.of(user));

        MvcResult response = mockMvc.perform(
            post("/user/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(handler().methodName("login")).andReturn();

        String jsonResponse = response.getResponse().getContentAsString();

        UserLoginWithValidator validated = gson.fromJson(
            jsonResponse,
            UserLoginWithValidator.class
        );

        assertNull(validated.getUsernameError());
        assertNull(validated.getPasswordError());
        assertTrue(validated.getIsValid());
    }

    @Test
    public void testInvalidLogin() throws Exception {
        Map<String,String> apa = new HashMap<>();
        apa.put("username", "fulan");
        apa.put("email", "fulan@fulan.id");
        apa.put("password", "dummy_password");
        apa.put("password2", "dummy_password");

        String json = new ObjectMapper().writeValueAsString(apa);

        mockMvc.perform(
            post("/user/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(handler().methodName("register"));

        apa = new HashMap<>();
        apa.put("username", "fulan2");
        apa.put("password", "dummy_password2");

        MvcResult response = mockMvc.perform(
            post("/user/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest())
            .andExpect(handler().methodName("login")).andReturn();

        String jsonResponse = response.getResponse().getContentAsString();

        UserLoginWithValidator validated = gson.fromJson(
            jsonResponse,
            UserLoginWithValidator.class
        );

        assertEquals("Incorrect username or password", validated.getInvalidLogin());
        assertFalse(validated.getIsValid());
    }

    @Test
    public void testValidateJwt() throws Exception {
        String token = "asd";

        when(jwt.validateToken(token)).thenReturn(true);

        MvcResult response = mockMvc.perform(
                post("/user/jwt/validate")
                .header("Authorization", "Bearer " + token)
            )
            .andExpect(status().isOk())
            .andExpect(handler().methodName("jwtValidate"))
            .andReturn();

        String jsonResponse = response.getResponse().getContentAsString();

        JwtResponse jwtResponse = gson.fromJson(jsonResponse, JwtResponse.class);

        assertTrue(jwtResponse.getIsValidated());
    }

    @Test
    public void testValidateInvalidJwt() throws Exception {
        String token = "asd";

        when(jwt.validateToken(token)).thenReturn(false);

        MvcResult response = mockMvc.perform(
                post("/user/jwt/validate")
                .header("Authorization", "Bearer " + token)
            )
            .andExpect(status().isBadRequest())
            .andExpect(handler().methodName("jwtValidate"))
            .andReturn();

        String jsonResponse = response.getResponse().getContentAsString();

        JwtResponse jwtResponse = gson.fromJson(jsonResponse, JwtResponse.class);

        assertFalse(jwtResponse.getIsValidated());
    }

    @Test
    public void testFetchMatchByInvalidJwt() throws Exception {
        String token = "asd";

        when(jwt.validateToken(token)).thenReturn(false);

        MvcResult response = mockMvc.perform(
                get("/user/match")
                .header("Authorization", "Bearer " + token)
            )
            .andExpect(status().isBadRequest())
            .andExpect(handler().methodName("fetchMatchByJwt"))
            .andReturn();

        String jsonResponse = response.getResponse().getContentAsString();

        FetchMatchResponse fetchMatchResponse = gson.fromJson(
            jsonResponse,
            FetchMatchResponse.class
        );

        assertEquals(0, fetchMatchResponse.getRoomId());
        assertNull(fetchMatchResponse.getUsername());
    }

    @Test
    public void testFetchMatchInvalidUsername() throws Exception {
        String token = "asd";

        when(jwt.validateToken(token)).thenReturn(true);
        when(jwt.getUsernameFromToken(token)).thenReturn("fulan");
        when(userRepositoryJPA.findByUsername("fulan")).thenReturn(Optional.empty());

        MvcResult response = mockMvc.perform(
                get("/user/match")
                .header("Authorization", "Bearer " + token)
            )
            .andExpect(status().isBadRequest())
            .andExpect(handler().methodName("fetchMatchByJwt"))
            .andReturn();

        String jsonResponse = response.getResponse().getContentAsString();

        FetchMatchResponse fetchMatchResponse = gson.fromJson(
            jsonResponse,
            FetchMatchResponse.class
        );

        assertEquals(0, fetchMatchResponse.getRoomId());
        assertNull(fetchMatchResponse.getUsername());
    }

    @Test
    public void testFetchMatchValid() throws Exception {
        String token = "asd";
        User user = new User(123L);
        user.setUsername("fulan");

        when(jwt.validateToken(token)).thenReturn(true);
        when(jwt.getUsernameFromToken(token)).thenReturn("fulan");
        when(userRepositoryJPA.findByUsername("fulan")).thenReturn(Optional.of(user));
        when(matchRepository.getMatchId(123L)).thenReturn(42);

        MvcResult response = mockMvc.perform(
                get("/user/match")
                .header("Authorization", "Bearer " + token)
            )
            .andExpect(status().isOk())
            .andExpect(handler().methodName("fetchMatchByJwt"))
            .andReturn();

        String jsonResponse = response.getResponse().getContentAsString();

        FetchMatchResponse fetchMatchResponse = gson.fromJson(
            jsonResponse,
            FetchMatchResponse.class
        );

        assertEquals(42, fetchMatchResponse.getRoomId());
        assertEquals("fulan", fetchMatchResponse.getUsername());
    }
}