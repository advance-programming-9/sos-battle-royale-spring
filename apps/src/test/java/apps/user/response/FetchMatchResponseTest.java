package apps.user.response;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class FetchMatchResponseTest {
    private FetchMatchResponse fetchMatchResponse;

    @BeforeEach
    public void setUp() {
        fetchMatchResponse = new FetchMatchResponse();
    }

    @Test
    public void testSetMatchId() {
        int matchId = 12;

        fetchMatchResponse.setRoomId(matchId);

        assertEquals(matchId, fetchMatchResponse.getRoomId());
    }

    @Test
    public void testSetUsername() {
        String username = "fulan";

        fetchMatchResponse.setUsername(username);

        assertEquals(username, fetchMatchResponse.getUsername());
    }

}