package apps.user.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MatchRepositoryTest {
    private MatchRepository matchRepository;

    @BeforeEach
    public void setUp() {
        matchRepository = new MatchRepository();
    }

    @Test
    public void testSetMatch() {
        long userId = 10L;
        int matchId = 11;

        matchRepository.setMatch(userId, matchId);

        assertEquals(matchId, matchRepository.getMatchId(userId));
        assertEquals(0L, matchRepository.getInGameScore(userId));
    }

    @Test
    public void testGetMatches() {
        List<Integer> matches = new ArrayList<>();
        List<Long> inGameScores = new ArrayList<>();

        matchRepository.setMatch(10L, 11);
        matches.add(11);
        inGameScores.add(0L);

        assertEquals(matches, matchRepository.getMatches());
        assertEquals(inGameScores, matchRepository.getInGameScores());
    }

    @Test
    public void testAddInGameScore() {
        long userId = 10L;

        matchRepository.setMatch(userId, 11);

        assertEquals(0L, matchRepository.getInGameScore(userId));

        matchRepository.addInGameScore(userId, 100L);

        assertEquals(100L, matchRepository.getInGameScore(userId));
    }

    @Test
    public void testRemoveMatchId() {
        long userId = 10L;

        matchRepository.setMatch(userId, 11);

        assertEquals(11, matchRepository.getMatchId(userId));
        assertEquals(0L, matchRepository.getInGameScore(userId));

        matchRepository.removeMatchId(userId);

        assertNull(matchRepository.getMatchId(userId));
        assertNull(matchRepository.getInGameScore(userId));
    }

    @Test
    public void testIsInAMatch() {
        long userId = 10L;

        assertFalse(matchRepository.isInAMatch(userId));

        matchRepository.setMatch(userId, 11);

        assertTrue(matchRepository.isInAMatch(userId));
    }

    @Test
    public void testIsInThisMatch() {
        long userId = 10L;
        int matchId = 11;

        assertFalse(matchRepository.isInThisMatch(userId, matchId));

        matchRepository.setMatch(userId, matchId);

        assertTrue(matchRepository.isInThisMatch(userId, matchId));
    }
}