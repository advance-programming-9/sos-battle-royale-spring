package apps.friends.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import apps.friends.core.FriendState;
import apps.friends.response.FriendsResponse;
import apps.room.core.Room;
import apps.room.core.RoomWithoutPassword;
import apps.room.core.base.SecuredRoom;
import apps.room.repository.RoomRepository;
import apps.user.auth.JWT;
import apps.user.model.User;
import apps.user.repository.MatchRepository;
import apps.user.repository.UserRepositoryJPA;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;


@WebMvcTest(controllers = FriendsController.class)
public class FriendsControllerTest {
    private final Gson gson = new Gson();

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MatchRepository matchRepository;

    @MockBean
    private UserRepositoryJPA userRepositoryJPA;

    @MockBean
    private RoomRepository roomRepository;

    @MockBean
    private JWT jwt;

    @Test
    public void testSuccesGetFriend() throws Exception {
        User user = new User(123L);
        user.setUsername("fulan");

        User dummy = new User(128L);
        dummy.setUsername("bulan");
        user.addFriend(dummy);
        String token = "asd";

        when(jwt.validateToken(token)).thenReturn(true);
        when(jwt.getUsernameFromToken(token)).thenReturn("fulan");
        when(userRepositoryJPA.findByUsername("fulan")).thenReturn(Optional.of(user));
        when(userRepositoryJPA.findByUsername("bulan")).thenReturn(Optional.of(dummy));

        MvcResult response = mockMvc.perform(
                get("/friends/fetch")
                        .header("Authorization", "Bearer " + token)
        )
                .andExpect(status().isOk())
                .andReturn();

        String jsonResponse = response.getResponse().getContentAsString();

        FriendsResponse friendResponse = gson.fromJson(
                jsonResponse,
                FriendsResponse.class
        );
        Set<String> usernameUserGetFriend = new HashSet<>();
        for (User friendUser: user.getFriends()) {
            usernameUserGetFriend.add(friendUser.getUsername());
        }

        Set<String> usernameUserFriendResponse = new HashSet<>();
        for (FriendState friendUser: friendResponse.getFriendState()) {
            usernameUserFriendResponse.add(friendUser.getName());
        }

        boolean userFriendResponse = usernameUserFriendResponse.containsAll(usernameUserGetFriend);
        boolean getFriendResponse = usernameUserGetFriend.containsAll(usernameUserFriendResponse);

        assertTrue(userFriendResponse && getFriendResponse);
    }

    @Test
    public void testSuccesGetFriendInRoom() throws Exception {
        User user = new User(123L);
        user.setUsername("fulan");

        User dummy = new User(128L);
        dummy.setUsername("bulan");
        user.addFriend(dummy);
        Room room = new Room("Judul");
        SecuredRoom securedRoom = new RoomWithoutPassword(room);
        when(matchRepository.getMatchId(128L)).thenReturn(23);
        when(roomRepository.getRoomById(23)).thenReturn(securedRoom);
        String token = "asd";

        when(jwt.validateToken(token)).thenReturn(true);
        when(jwt.getUsernameFromToken(token)).thenReturn("fulan");
        when(userRepositoryJPA.findByUsername("fulan")).thenReturn(Optional.of(user));
        when(userRepositoryJPA.findByUsername("bulan")).thenReturn(Optional.of(dummy));

        MvcResult response = mockMvc.perform(
                get("/friends/fetch")
                        .header("Authorization", "Bearer " + token)
        )
                .andExpect(status().isOk())
                .andReturn();

        String jsonResponse = response.getResponse().getContentAsString();

        FriendsResponse friendResponse = gson.fromJson(
                jsonResponse,
                FriendsResponse.class
        );
        Set<String> usernameUserGetFriend = new HashSet<>();
        for (User friendUser: user.getFriends()) {
            usernameUserGetFriend.add(friendUser.getUsername());
        }

        Set<String> usernameUserFriendResponse = new HashSet<>();
        for (FriendState friendUser: friendResponse.getFriendState()) {
            usernameUserFriendResponse.add(friendUser.getName());
        }

        boolean userFriendResponse = usernameUserFriendResponse.containsAll(usernameUserGetFriend);
        boolean getFriendResponse = usernameUserGetFriend.containsAll(usernameUserFriendResponse);

        assertTrue(userFriendResponse && getFriendResponse);


    }

    @Test
    public void testFailJWTGetFriend() throws Exception {
        User user = new User(123L);
        user.setUsername("fulan");

        User dummy = new User(12L);
        dummy.setUsername("bulan");
        user.addFriend(dummy);
        String token = "asd";

        MvcResult response = mockMvc.perform(
                get("/friends/fetch")
                        .header("Authorization", "Bearer " + token)
        )
                .andExpect(status().isBadRequest())
                .andReturn();

        String jsonResponse = response.getResponse().getContentAsString();

        FriendsResponse friendResponse = gson.fromJson(
                jsonResponse,
                FriendsResponse.class
        );

        assertEquals("Authorization Error!",friendResponse.getError());
    }

    @Test
    public void testFailUsernameGetFriend() throws Exception {
        String token = "asd";

        when(jwt.validateToken(token)).thenReturn(true);
        when(jwt.getUsernameFromToken(token)).thenReturn("fulan");
        when(userRepositoryJPA.findByUsername("fulan")).thenReturn(Optional.empty());

        MvcResult response = mockMvc.perform(
                get("/friends/fetch")
                        .header("Authorization", "Bearer " + token)
        )
                .andExpect(status().isBadRequest())
                .andReturn();

        String jsonResponse = response.getResponse().getContentAsString();

        FriendsResponse friendResponse = gson.fromJson(
                jsonResponse,
                FriendsResponse.class
        );

        assertEquals("Username invalid",friendResponse.getError());
        assertNull(friendResponse.getUsername());
    }

    @Test
    public void testSuccesSearchFriend() throws Exception {
        User user = new User(123L);
        user.setUsername("fulan");

        User dummy = new User(128L);
        dummy.setUsername("bulan");
        String token = "asd";

        when(jwt.validateToken(token)).thenReturn(true);
        when(jwt.getUsernameFromToken(token)).thenReturn("fulan");
        when(userRepositoryJPA.findByUsername("fulan")).thenReturn(Optional.of(user));
        when(userRepositoryJPA.findByUsername("bulan")).thenReturn(Optional.of(dummy));

        Map<String,String> mapFriendRequest = new HashMap<>();
        mapFriendRequest.put("friendUsername", "bulan");
        String requestJson = new ObjectMapper().writeValueAsString(mapFriendRequest);

        MvcResult response = mockMvc.perform(
                post("/friends/find")
                        .header("Authorization", "Bearer " + token)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson)
                        .accept(MediaType.APPLICATION_JSON)

        )
                .andExpect(status().isOk())
                .andReturn();

        String jsonResponse = response.getResponse().getContentAsString();

        Map<String, Object> findResponse = gson.fromJson(
                jsonResponse,
                Map.class
        );

        assertEquals("bulan",findResponse.get("username"));
    }

    @Test
    public void testFailJWTFindFriend() throws Exception {
        User user = new User(123L);
        user.setUsername("fulan");

        User dummy = new User(12L);
        dummy.setUsername("bulan");
        String token = "asd";

        Map<String,String> mapFriendRequest = new HashMap<>();
        mapFriendRequest.put("friendUsername", "bulan");
        String requestJson = new ObjectMapper().writeValueAsString(mapFriendRequest);

        MvcResult response = mockMvc.perform(
                post("/friends/find")
                        .header("Authorization", "Bearer " + token)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson)
                        .accept(MediaType.APPLICATION_JSON)

        )
                .andExpect(status().isBadRequest())
                .andReturn();

        String jsonResponse = response.getResponse().getContentAsString();

        FriendsResponse friendResponse = gson.fromJson(
                jsonResponse,
                FriendsResponse.class
        );

        assertEquals("Authorization Error!",friendResponse.getError());
    }

    @Test
    public void testFailUsernameFindFriend() throws Exception {
        String token = "asd";

        when(jwt.validateToken(token)).thenReturn(true);
        when(jwt.getUsernameFromToken(token)).thenReturn("fulan");
        when(userRepositoryJPA.findByUsername("fulan")).thenReturn(Optional.empty());

        Map<String,String> mapFriendRequest = new HashMap<>();
        mapFriendRequest.put("friendUsername", "bulan");
        String requestJson = new ObjectMapper().writeValueAsString(mapFriendRequest);

        MvcResult response = mockMvc.perform(
                post("/friends/find")
                        .header("Authorization", "Bearer " + token)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andReturn();

        String jsonResponse = response.getResponse().getContentAsString();

        FriendsResponse friendResponse = gson.fromJson(
                jsonResponse,
                FriendsResponse.class
        );

        assertEquals("Username invalid",friendResponse.getError());
        assertNull(friendResponse.getUsername());
    }

    @Test
    public void testPlayerNotFound() throws Exception {
        User user = new User(123L);
        user.setUsername("fulan");
        String token = "asd";

        when(jwt.validateToken(token)).thenReturn(true);
        when(jwt.getUsernameFromToken(token)).thenReturn("fulan");
        when(userRepositoryJPA.findByUsername("fulan")).thenReturn(Optional.of(user));
        when(userRepositoryJPA.findByUsername("bulan")).thenReturn(Optional.empty());

        Map<String,String> mapFriendRequest = new HashMap<>();
        mapFriendRequest.put("friendUsername", "bulan");
        String requestJson = new ObjectMapper().writeValueAsString(mapFriendRequest);

        MvcResult response = mockMvc.perform(
                post("/friends/find")
                        .header("Authorization", "Bearer " + token)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andReturn();

        String jsonResponse = response.getResponse().getContentAsString();

        FriendsResponse friendResponse = gson.fromJson(
                jsonResponse,
                FriendsResponse.class
        );

        assertEquals("Player not found!",friendResponse.getError());
    }

    @Test
    public void testSuccesAddFriend() throws Exception {
        User user = new User(123L);
        user.setUsername("fulan");

        User dummy = new User(128L);
        dummy.setUsername("bulan");
        String token = "asd";

        when(jwt.validateToken(token)).thenReturn(true);
        when(jwt.getUsernameFromToken(token)).thenReturn("fulan");
        when(userRepositoryJPA.findByUsername("fulan")).thenReturn(Optional.of(user));
        when(userRepositoryJPA.findByUsername("bulan")).thenReturn(Optional.of(dummy));

        Map<String,String> mapFriendRequest = new HashMap<>();
        mapFriendRequest.put("friendUsername", "bulan");
        String requestJson = new ObjectMapper().writeValueAsString(mapFriendRequest);

        MvcResult response = mockMvc.perform(
                put("/friends/add")
                        .header("Authorization", "Bearer " + token)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson)
                        .accept(MediaType.APPLICATION_JSON)

        )
                .andExpect(status().isOk())
                .andReturn();

        String jsonResponse = response.getResponse().getContentAsString();

        Map<String, Object> findResponse = gson.fromJson(
                jsonResponse,
                Map.class
        );

        assertEquals("bulan",findResponse.get("username"));
    }

    @Test
    public void testFailJWTAddFriend() throws Exception {
        User user = new User(123L);
        user.setUsername("fulan");

        User dummy = new User(12L);
        dummy.setUsername("bulan");
        String token = "asd";

        Map<String,String> mapFriendRequest = new HashMap<>();
        mapFriendRequest.put("friendUsername", "bulan");
        String requestJson = new ObjectMapper().writeValueAsString(mapFriendRequest);

        MvcResult response = mockMvc.perform(
                put("/friends/add")
                        .header("Authorization", "Bearer " + token)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson)
                        .accept(MediaType.APPLICATION_JSON)

        )
                .andExpect(status().isBadRequest())
                .andReturn();

        String jsonResponse = response.getResponse().getContentAsString();

        FriendsResponse friendResponse = gson.fromJson(
                jsonResponse,
                FriendsResponse.class
        );

        assertEquals("Authorization Error!",friendResponse.getError());
    }

    @Test
    public void testFailUsernameAddFriend() throws Exception {
        String token = "asd";

        when(jwt.validateToken(token)).thenReturn(true);
        when(jwt.getUsernameFromToken(token)).thenReturn("fulan");
        when(userRepositoryJPA.findByUsername("fulan")).thenReturn(Optional.empty());

        Map<String,String> mapFriendRequest = new HashMap<>();
        mapFriendRequest.put("friendUsername", "bulan");
        String requestJson = new ObjectMapper().writeValueAsString(mapFriendRequest);

        MvcResult response = mockMvc.perform(
                put("/friends/add")
                        .header("Authorization", "Bearer " + token)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andReturn();

        String jsonResponse = response.getResponse().getContentAsString();

        FriendsResponse friendResponse = gson.fromJson(
                jsonResponse,
                FriendsResponse.class
        );

        assertEquals("Username invalid",friendResponse.getError());
        assertNull(friendResponse.getUsername());
    }

    @Test
    public void testAddPlayerNotFound() throws Exception {
        User user = new User(123L);
        user.setUsername("fulan");
        String token = "asd";

        when(jwt.validateToken(token)).thenReturn(true);
        when(jwt.getUsernameFromToken(token)).thenReturn("fulan");
        when(userRepositoryJPA.findByUsername("fulan")).thenReturn(Optional.of(user));
        when(userRepositoryJPA.findByUsername("bulan")).thenReturn(Optional.empty());

        Map<String,String> mapFriendRequest = new HashMap<>();
        mapFriendRequest.put("friendUsername", "bulan");
        String requestJson = new ObjectMapper().writeValueAsString(mapFriendRequest);

        MvcResult response = mockMvc.perform(
                put("/friends/add")
                        .header("Authorization", "Bearer " + token)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andReturn();

        String jsonResponse = response.getResponse().getContentAsString();

        FriendsResponse friendResponse = gson.fromJson(
                jsonResponse,
                FriendsResponse.class
        );

        assertEquals("Player not found!",friendResponse.getError());
    }
}