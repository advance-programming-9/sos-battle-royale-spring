package apps.friends.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class FriendsCoreTest {
    Friends friends;

    @BeforeEach
    public void setUp() {
        friends = new Friends(1L);
    }

    @Test
    public void testAddFriend() {
        friends.addFriend(2L);
        ArrayList<Long> listOfFriendAfterAddFriend = friends.getFriends();
        ArrayList<Long> expectedListOfFriendAfterAddFriend = new ArrayList<Long>();
        expectedListOfFriendAfterAddFriend.add(2L);
        assertEquals(listOfFriendAfterAddFriend,expectedListOfFriendAfterAddFriend);
    }

    @Test
    public void testDeleteFriend() {
        friends.deleteFriend(2L);
        assertEquals(friends.getFriends(),new ArrayList<Long>());
    }

    @Test
    public void testGetIdUser() {
        assertEquals(friends.getUserId(),1L);
    }
}