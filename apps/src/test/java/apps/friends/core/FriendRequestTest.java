package apps.friends.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class FriendRequestTest {

    private FriendsRequest friendRequest;

    @BeforeEach
    public void setUp() {
        friendRequest = new FriendsRequest();
    }

    @Test
    public void testSetFriend() {
        friendRequest.setFriendUsername("bumi");

        assertEquals("bumi",friendRequest.getFriendUsername());
    }
}
