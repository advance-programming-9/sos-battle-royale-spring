package apps.friends.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class FriendStateTest {
    FriendState friendState;

    @BeforeEach
    public void setUp() {
        friendState = new FriendState();
    }

    @Test
    public void testConstructor() {
        assertEquals(friendState.getId(),100);
        assertEquals(friendState.getRoomId(),0);
        assertEquals(friendState.getName(),"dummy");
        assertEquals(friendState.getState(),"-");

        FriendState testConstructor = new FriendState("bla",12L,12,"playing");
        assertEquals(testConstructor.getName(),"bla");
        assertEquals(testConstructor.getRoomId(),12);
        assertEquals(testConstructor.getState(),"playing");
        assertEquals(testConstructor.getId(),12L);
    }

    @Test
    public void testSetterGetter() {
        friendState.setName("Aji");
        assertEquals(friendState.getName(),"Aji");
        friendState.setId(10L);
        assertEquals(friendState.getId(),10L);
        friendState.setRoomId(1);
        assertEquals(friendState.getRoomId(),1);
        friendState.setState("In Room");
        assertEquals(friendState.getState(),"In Room");
    }
}