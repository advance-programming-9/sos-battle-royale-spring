package apps.friends.response;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import apps.scoreboard.core.adapter.SafeUser;
import apps.user.model.User;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class FriendsResponseTest {

    private FriendsResponse friendResponse;

    @BeforeEach
    public void setUp() {
        friendResponse = new FriendsResponse();
    }

    @Test
    public void testSetFriend() {
        Set<User> friend = new HashSet<>();
        User dummy = new User();
        User dummy2 = new User();
        dummy.setUsername("lolo");

        friend.add(dummy);
        friend.add(dummy2);

        friendResponse.setUsername(dummy.getUsername());
        friendResponse.setFriend(friend);
        friendResponse.setError("User Id cannot be found");

        Set<String> usernameUserGetFriend = new HashSet<>();
        for (User friendUser: friend) {
            usernameUserGetFriend.add(friendUser.getUsername());
        }

        Set<String> usernameUserFriendResponse = new HashSet<>();
        for (SafeUser friendUser: friendResponse.getFriend()) {
            usernameUserFriendResponse.add(friendUser.getUsername());
        }

        boolean userFriendResponse = usernameUserFriendResponse.containsAll(usernameUserGetFriend);
        boolean getFriendResponse = usernameUserGetFriend.containsAll(usernameUserFriendResponse);

        assertEquals(dummy.getUsername(),"lolo");
        assertTrue(userFriendResponse && getFriendResponse);
        assertEquals("User Id cannot be found",friendResponse.getError());
    }
}
