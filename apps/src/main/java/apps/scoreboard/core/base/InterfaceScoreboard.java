package apps.scoreboard.core.base;

import apps.user.model.User;
import java.util.List;

/**
 * InterfaceScoreboard.
 */
public interface InterfaceScoreboard {

    void update(User user);

    List<User> getTopTen();

}