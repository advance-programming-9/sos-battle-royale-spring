package apps.scoreboard.core;

import apps.scoreboard.core.base.InterfaceScoreboard;
import apps.user.model.User;
import apps.user.repository.UserRepositoryJPA;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Scoreboard.
 */
@Repository
public class Scoreboard implements InterfaceScoreboard {

    @Autowired
    private final UserRepositoryJPA userRepositoryJPA;

    public List<User> topTenUser;

    public Scoreboard(UserRepositoryJPA userRepositoryJPA) {
        this.userRepositoryJPA = userRepositoryJPA;
        this.update();
    }

    @Override
    public void update(User user) {
        userRepositoryJPA.updateScoreById(user.getId(), user.getScore());
        this.update();
    }

    private void update() {
        topTenUser = userRepositoryJPA.findByOrderByScoreDescLimit10();
    }

    @Override
    public List<User> getTopTen() {
        return this.topTenUser;
    }
}