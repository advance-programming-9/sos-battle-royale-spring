package apps.scoreboard.core.adapter;

import apps.bot.core.Bot;
import apps.user.model.User;

/**
 * SafeUser.
 */
public class SafeUser implements Comparable<SafeUser> {

    private User user;
    private long inGameScore = 0;
    private Bot bot;

    public SafeUser(User user) {
        this.user = user;
    }

    public SafeUser(Bot bot) {
        this.bot = bot;
    }

    /**
     * return either bot's or user's score.
     * @return long score
     */
    public long getScore() {
        if (this.user == null) {
            return 0;
        }
        return user.getScore();
    }

    /**
     * return either bot's or user's username.
     * @return String username
     */
    public String getUsername() {
        if (this.user == null) {
            return bot.getUsername();
        }
        return user.getUsername();
    }

    public long getInGameScore() {
        return inGameScore;
    }

    public void setInGameScore(long inGameScore) {
        this.inGameScore = inGameScore;
    }

    /**
     * Compare SafeUser inGameScore and another SafeUser inGameScore.
     * @return int within 3 probability.
     */
    @Override
    public int compareTo(SafeUser safeUser) {
        if (this.getInGameScore() > safeUser.getInGameScore()) {
            return -1;
        } else if (this.getInGameScore() < safeUser.getInGameScore()) {
            return 1;
        }

        return 0;
    }
}