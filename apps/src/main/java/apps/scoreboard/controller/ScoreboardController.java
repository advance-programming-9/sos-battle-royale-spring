package apps.scoreboard.controller;

import apps.scoreboard.core.Scoreboard;
import apps.scoreboard.core.adapter.SafeUser;
import apps.user.auth.JwtAuthorization;
import apps.user.model.User;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/scoreboard")
@CrossOrigin(origins = { "http://localhost:21483", "http://localhost:3000", "https://sos.ariqbasyar.id" })
public class ScoreboardController {

    @Autowired
    private final Scoreboard scoreboard;

    public ScoreboardController(Scoreboard scoreboard) {
        this.scoreboard = scoreboard;
    }

    /**
     * get mapping for fetch the scoreboard.
     * @param jwtAuthorization request header authorization
     */
    @GetMapping("/fetch")
    public ResponseEntity<SafeUser[]> fetchScoreboard(
        @RequestHeader(name = "Authorization") JwtAuthorization jwtAuthorization
    ) {
        SafeUser[] safeUsers = new SafeUser[10];

        String error = jwtAuthorization.getError();
        if (error != null) {
            return ResponseEntity.badRequest().body(safeUsers);
        }

        List<User> users = scoreboard.getTopTen();
        for (int index = 0; index < users.size(); index++) {
            safeUsers[index] = new SafeUser(users.get(index));
        }

        return ResponseEntity.ok(safeUsers);
    }
}