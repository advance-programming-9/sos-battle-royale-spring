package apps.game.controller;

import apps.bot.repository.BotRepository;
import apps.room.core.RequestRoomId;
import apps.room.core.RoomResponse;
import apps.room.core.base.SecuredRoom;
import apps.room.repository.RoomRepository;
import apps.user.auth.JwtAuthorization;
import apps.user.repository.MatchRepository;
import apps.user.repository.UserRepositoryJPA;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/game")
@CrossOrigin(origins = { "http://localhost:21483", "http://localhost:3000", "https://sos.ariqbasyar.id" })
public class GameController {

    Logger logger = LoggerFactory.getLogger(GameController.class);

    @Autowired
    private final RoomRepository roomRepository;

    @Autowired
    private final UserRepositoryJPA userRepositoryJPA;

    @Autowired
    private final BotRepository botRepository;

    @Autowired
    private final MatchRepository matchRepository;

    @Autowired
    private final SimpMessagingTemplate simpMessagingTemplate;

    /**
     * contruct the game controller with given autowired.
     * @param roomRepository autorwired
     * @param userRepositoryJPA autorwired
     * @param matchRepository autorwired
     * @param simpMessagingTemplate autorwired
     */
    public GameController(
        RoomRepository roomRepository, 
        UserRepositoryJPA userRepositoryJPA,
        MatchRepository matchRepository, 
        SimpMessagingTemplate simpMessagingTemplate,
        BotRepository botRepository
    ) {
        this.roomRepository = roomRepository;
        this.userRepositoryJPA = userRepositoryJPA;
        this.matchRepository = matchRepository;
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.botRepository = botRepository;
        setUp();
    }

    public void setUp() {
        RoomResponse.setUserRepositoryJPA(userRepositoryJPA, matchRepository,botRepository);
    }

    /**
     * post mapping for start the game with given room id.
     * @param jwtAuthorization request header authorization
     * @param requestRoomId input room id
     */
    @PostMapping("/start")
    public void startGame(
        @RequestHeader(name = "Authorization") JwtAuthorization jwtAuthorization,
        @RequestBody RequestRoomId requestRoomId
    ) {
        int roomId = requestRoomId.getRoomId();
        String error = jwtAuthorization.getError();
        if (error != null) {
            return;
        }

        SecuredRoom securedRoom = roomRepository.getRoomById(roomId);
        if (securedRoom == null) {
            return;
        }

        String username = jwtAuthorization.getUsername();

        RoomResponse roomResponse = securedRoom.getRoomResponse();
        if (!roomResponse.getRoomMaster().equals(username)) {
            return;
        }
        securedRoom.start();
        simpMessagingTemplate.convertAndSend("/game/room/" + roomId,
                roomResponse);
    }
}