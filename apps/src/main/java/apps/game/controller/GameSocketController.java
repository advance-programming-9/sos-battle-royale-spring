package apps.game.controller;

import apps.bot.core.Bot;
import apps.bot.repository.BotRepository;
import apps.game.core.arena.Arena;
import apps.game.core.arena.GridRequest;
import apps.room.core.RoomResponse;
import apps.room.core.base.SecuredRoom;
import apps.room.repository.RoomRepository;
import apps.user.auth.JWT;
import apps.user.auth.JwtAuthorization;
import apps.user.model.User;
import apps.user.model.validator.atomic.UsernameValidator;
import apps.user.repository.MatchRepository;
import apps.user.repository.UserRepositoryJPA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GameSocketController {
    @Autowired
    private final JWT jwt;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    private final RoomRepository roomRepository;

    @Autowired
    private final MatchRepository matchRepository;

    @Autowired
    private final UserRepositoryJPA userRepositoryJPA;

    @Autowired
    private final BotRepository botRepository;

    /**
     * construct the game socket controller with given autowired.
     * @param roomRepository autowired
     * @param matchRepository autowired
     * @param userRepositoryJPA autowired
     */
    public GameSocketController(
        JWT jwt,
        RoomRepository roomRepository,
        MatchRepository matchRepository,
        UserRepositoryJPA userRepositoryJPA,
        BotRepository botRepository
    ) {
        this.jwt = jwt;
        this.matchRepository = matchRepository;
        this.roomRepository = roomRepository;
        this.userRepositoryJPA = userRepositoryJPA;
        this.botRepository = botRepository;
    }

    /**
     * message mapping to send grid.
     * @param gridRequest conert request body to an instance
     * @param roomId input room id
     */
    @MessageMapping("/game/room/{roomId}")
    public void sendGrid(
        @RequestParam GridRequest gridRequest,
        @DestinationVariable Integer roomId,
        @Header(value = "Authorization") String authorization
    ) {
        JwtAuthorization jwtAuthorization = new JwtAuthorization(jwt);
        jwtAuthorization.setAuthorization(authorization);
        String errorJwt = jwtAuthorization.getError();
        if (errorJwt != null) {
            return;
        }

        SecuredRoom securedRoom = roomRepository.getRoomById(roomId);
        RoomResponse roomResponse = securedRoom.getRoomResponse();
        String username = jwtAuthorization.getUsername();
        User user = userRepositoryJPA.findByUsername(username).get();

        if (!matchRepository.isInThisMatch(user.getId(), securedRoom.getRoomId())) {
            return;
        }

        long score = securedRoom.click(gridRequest);
        matchRepository.addInGameScore(user.getId(), score);

        simpMessagingTemplate.convertAndSend("/game/room/" + roomId, roomResponse);
        botMove(securedRoom);
    }

    /**
     * message mapping for pass turn.
     * @param roomId input for room id
     */
    @MessageMapping("/game/pass/{roomId}")
    public void passTurn(
        @DestinationVariable Integer roomId,
        @Header(value = "Authorization") String authorization
    ) {
        JwtAuthorization jwtAuthorization = new JwtAuthorization(jwt);
        jwtAuthorization.setAuthorization(authorization);
        String errorJwt = jwtAuthorization.getError();
        if (errorJwt != null) {
            return;
        }
        SecuredRoom securedRoom = roomRepository.getRoomById(roomId);
        String username = jwtAuthorization.getUsername();
        User user = userRepositoryJPA.findByUsername(username).get();

        if (!matchRepository.isInThisMatch(user.getId(), securedRoom.getRoomId())) {
            return;
        }

        securedRoom.passTurn(user.getId());

        RoomResponse roomResponse = securedRoom.getRoomResponse();
        simpMessagingTemplate.convertAndSend("/game/room/" + roomId, roomResponse);
        botMove(securedRoom);
    }

    /**
     * controller when room master request add bot.
     * @param roomId related room id
     * @param authorization string jwt token
     */
    @MessageMapping("/game/bot/add/{roomId}")
    public void enterBot(
        @DestinationVariable Integer roomId,
        @Header(value = "Authorization") String authorization
    ) {
        JwtAuthorization jwtAuthorization = new JwtAuthorization(jwt);
        jwtAuthorization.setAuthorization(authorization);
        String errorJwt = jwtAuthorization.getError();
        if (errorJwt != null) {
            return;
        }
        SecuredRoom securedRoom = roomRepository.getRoomById(roomId);
        String username = jwtAuthorization.getUsername();
        User user = userRepositoryJPA.findByUsername(username).get();

        if (!matchRepository.isInThisMatch(user.getId(), securedRoom.getRoomId())) {
            return;
        }

        Bot bot = new Bot();
        boolean canAddBot = securedRoom.enterBot(user, bot);
        if (!canAddBot) {
            return;
        }

        botRepository.enterBot(bot);
        matchRepository.setMatch(bot.getId(), securedRoom.getRoomId());

        RoomResponse roomResponse = securedRoom.getRoomResponse();
        simpMessagingTemplate.convertAndSend("/game/room/" + roomId, roomResponse);
    }

    /**
     * controller when room master request delete bot.
     * @param roomId related room id
     * @param authorization string jwt token
     */
    @MessageMapping("/game/bot/delete/{roomId}")
    public void deleteBot(
        @DestinationVariable Integer roomId,
        @Header(value = "Authorization") String authorization
    ) {
        JwtAuthorization jwtAuthorization = new JwtAuthorization(jwt);
        jwtAuthorization.setAuthorization(authorization);
        String errorJwt = jwtAuthorization.getError();
        if (errorJwt != null) {
            return;
        }
        SecuredRoom securedRoom = roomRepository.getRoomById(roomId);
        String username = jwtAuthorization.getUsername();
        User user = userRepositoryJPA.findByUsername(username).get();
        
        if (!matchRepository.isInThisMatch(user.getId(), securedRoom.getRoomId())) {
            return;
        }
        
        Long deletedBotId = securedRoom.deleteBot(user);
        if (deletedBotId == -1L) {
            return;
        }
        matchRepository.removeMatchId(deletedBotId);

        RoomResponse roomResponse = securedRoom.getRoomResponse();
        simpMessagingTemplate.convertAndSend("/game/room/" + roomId, roomResponse);
    }

    /**
     * bot's move right after user's move.
     * @param securedRoom bot in secured room
     */
    public void botMove(
        SecuredRoom securedRoom
    ) {
        RoomResponse roomResponse = securedRoom.getRoomResponse();
        String username = roomResponse.getWhoIsNext();
        if (!UsernameValidator.isBotUsername(username)
            || !roomResponse.getState().equals("playing")) {
            return;
        }

        Long botId = securedRoom.getIdWhoIsNext();
        Bot bot = botRepository.getBotById(botId);

        Arena arena = roomResponse.getArena();
        GridRequest gridRequest = bot.findBestMove(arena);

        long score = securedRoom.click(gridRequest);
        matchRepository.addInGameScore(botId, score);

        int roomId = securedRoom.getRoomId();
        simpMessagingTemplate.convertAndSend("/game/room/" + roomId, roomResponse);
        if (securedRoom.getIdWhoIsNext() < -1) {
            botMove(securedRoom);
        }
    }
}