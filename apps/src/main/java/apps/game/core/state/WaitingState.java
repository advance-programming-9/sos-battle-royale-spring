package apps.game.core.state;

import apps.bot.core.Bot;
import apps.game.core.Game;
import apps.user.model.User;

public class WaitingState extends GameState {

    public WaitingState(Game game) {
        super(game);
    }

    @Override
    public boolean enter(User user) {
        return game.addUser(user);
    }

    @Override
    public boolean enter(Bot bot) {
        return game.addBot(bot);
    }

    @Override
    public boolean start() {
        if (game.getUsers().size() < 2) {
            return false;
        }
        game.setState(new PlayingState(game));
        return true;
    }

    @Override
    public boolean finish() {
        return false;
    }

    @Override
    public boolean quit(User user) {
        return game.removeUser(user);
    }

    @Override
    public long click(int x, int y, char character) {
        return 0;
    }

    @Override
    public String toString() {
        return "waiting";
    }

    @Override
    public Long deleteBot() {
        for (Long userId: this.game.getUsers()) {
            if (userId < -1) {
                return this.game.removeBot(userId);
            }
        }
        return -1L;
    }
}