package apps.game.core.state;

import apps.bot.core.Bot;
import apps.game.core.Game;
import apps.user.model.User;

public class PlayingState extends GameState {

    public PlayingState(Game game) {
        super(game);
    }

    @Override
    public boolean enter(User user) {
        if (game.getUsers().indexOf(user.getId()) != -1) {
            return true;
        }
        return false;
    }

    @Override
    public boolean enter(Bot bot) {
        return false;
    }

    @Override
    public boolean start() {
        return false;
    }

    @Override
    public boolean quit(User user) {
        return game.removeUser(user);
    }

    @Override
    public long click(int x, int y, char character) {
        long score = game.setGrid(x, y, character);
        return score;
    }

    @Override
    public boolean finish() {
        game.setState(new FinishedState(game));
        return true;
    }

    @Override
    public String toString() {
        return "playing";
    }

    @Override
    public Long deleteBot() {
        return -1L;
    }
}