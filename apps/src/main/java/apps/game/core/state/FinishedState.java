package apps.game.core.state;

import apps.bot.core.Bot;
import apps.game.core.Game;
import apps.user.model.User;

public class FinishedState extends GameState {

    public FinishedState(Game game) {
        super(game);
    }

    @Override
    public boolean enter(User user) {
        if (game.getUsers().indexOf(user.getId()) != -1) {
            return true;
        }
        return false;
    }

    @Override
    public boolean enter(Bot bot) {
        return false;
    }

    @Override
    public boolean start() {
        return false;
    }

    @Override
    public boolean finish() {
        return false;
    }

    @Override
    public boolean quit(User user) {
        return game.removeUser(user);
    }

    @Override
    public long click(int x, int y, char character) {
        return 0;
    }

    @Override
    public String toString() {
        return "finished";
    }

    @Override
    public Long deleteBot() {
        return -1L;
    }

}