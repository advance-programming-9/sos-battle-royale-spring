package apps.game.core.state;

import apps.bot.core.Bot;
import apps.game.core.Game;
import apps.user.model.User;

public abstract class GameState {
    protected Game game;

    public GameState(Game game) {
        this.game = game;
    }

    public abstract boolean enter(User user);

    public abstract boolean enter(Bot bot);

    public abstract boolean start();

    public abstract boolean finish();

    public abstract boolean quit(User user);

    public abstract long click(int x, int y, char character);

    public abstract String toString();

    public abstract Long deleteBot(); 
}