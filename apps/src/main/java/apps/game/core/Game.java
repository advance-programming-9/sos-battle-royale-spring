package apps.game.core;

import apps.bot.core.Bot;
import apps.game.core.arena.Arena;
import apps.game.core.state.GameState;
import apps.game.core.state.WaitingState;
import apps.user.model.User;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Game {
    private Arena gameField = new Arena(10, 10);
    private List<Long> usersIdList = new ArrayList<>();
    private long whoIsNext = -1;
    private GameState gameState;

    public Game() {
        this.gameState = new WaitingState(this);
    }

    public boolean getIsProtected() {
        return false;
    }

    public Arena getArena() {
        return gameField;
    }

    /**
     * return <code>false</code> when the user is not found.
     * 
     * @param user user that want to be removed
     * @return boolean result whether the removal success or not
     */
    public boolean removeUser(User user) {
        long userId = user.getId();
        return usersIdList.remove(userId);
    }

    public boolean quit(User user) {
        return gameState.quit(user);
    }

    /**
     * start the game.
     * 
     * @return true if in waiting state
     */
    public boolean start() {
        boolean status = gameState.start();
        if (status == true) {
            updateWhoIsNext();
            return status;
        }
        return false;
    }

    private boolean getIsFinished() {
        int width = gameField.getWidth();
        int height = gameField.getHeight();

        for (int x = 0; x < height; x++) {
            for (int y = 0; y < width; y++) {
                if (gameField.getStats(x, y) == 'X') {
                    return false;
                }
            }
        }

        return true;
    }

    private void checkIsFinished() {
        boolean isFinished = getIsFinished();
        if (isFinished) {
            finish();
        }
    }

    public boolean finish() {
        return gameState.finish();
    }

    /**
     * return <code>false</code> when the game is full.
     * 
     * @param user user that want to be added
     * @return boolean boolean result whether the addition success or not
     */
    public boolean addUser(User user) {
        if (usersIdList.indexOf(user.getId()) != -1) {
            return true;
        }
        if (usersIdList.size() < 10) {
            usersIdList.add(user.getId());
            return true;
        }
        return false;
    }

    public boolean enter(User user) {
        return gameState.enter(user);
    }

    /**
     * user passing turn.
     * 
     * @param userId for validating with whoIsNext
     */
    public void passTurn(long userId) {
        if (this.whoIsNext != userId) {
            return;
        }
        updateWhoIsNext();
    }

    private void updateWhoIsNext() {
        int current = usersIdList.indexOf(whoIsNext);
        whoIsNext = usersIdList.get((current + 1) % usersIdList.size());
    }

    /**
     * return first person as a room master.
     * 
     * @return room master's id
     */
    public long getRoomMaster() {
        for (long userId: usersIdList) {
            if (userId > 0) {
                return userId;
            }
        }
        return -1L;
    }

    public int getCountPeople() {
        return usersIdList.size();
    }

    public long getWhoIsNext() {
        return whoIsNext;
    }

    public List<Long> getUsers() {
        return usersIdList;
    }

    public void setState(GameState gameState) {
        this.gameState = gameState;
    }

    /**
     * set character to a grid from given x and y.
     * 
     * @param x         for the index x
     * @param y         for the index y
     * @param character set the character
     */
    public long setGrid(int x, int y, char character) {
        long score = this.gameField.setGrid(x, y, character);
        updateWhoIsNext();
        checkIsFinished();
        return score;
    }

    public long click(int x, int y, char character) {
        return gameState.click(x, y, character);
    }

    public String getState() {
        return gameState.toString();
    }

    /**
     * add bot to the game.
     * @param bot given bot
     * @return boolean if bot successfully added to the game
     */
    public boolean addBot(Bot bot) {
        if (!(usersIdList.size() < 10)) {
            return false;
        }
        usersIdList.add((long) bot.getId());
        return true;
    }

    public boolean enterBot(Bot bot) {
        return gameState.enter(bot);
    }

    public Long deleteBot() {
        return gameState.deleteBot();
    }

    public Long removeBot(Long botId) {
        return usersIdList.remove(botId) ? botId : -1L;
    }

    /**
     * is room empty check if there are still users left.
     * @return boolean true if there are no users left
     */
    public boolean isRoomEmpty() {
        return getOnlyUsersId().size() == 0;
    }

    /**
     * get all bots id from users id list with filter id > 0.
     * @return list of bots id
     */
    public List<Long> getAllBotsId() {
        return usersIdList.stream()
            .filter(id -> id <= 0)
            .collect(Collectors.toList());
    }

    private List<Long> getOnlyUsersId() {
        return usersIdList.stream()
            .filter(id -> id > 0)
            .collect(Collectors.toList());
    }
}
