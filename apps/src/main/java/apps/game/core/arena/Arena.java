package apps.game.core.arena;

public class Arena {
    private Grid[][] field;
    private char lastChar;
    private int height;
    private int width;

    /**
     * construct the arena with given x and y.
     * 
     * @param x for the width of board
     * @param y for the height of board
     */
    public Arena(int x, int y) {
        this.lastChar = 'O';
        this.field = new Grid[x][y];
        this.height = x;
        this.width = y;
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                this.field[i][j] = new Grid();
            }
        }
    }

    public char getLastChar() {
        return lastChar;
    }

    public int getHeight() {
        return this.height;
    }

    public int getWidth() {
        return this.width;
    }

    public Grid getGrid(int x, int y) {
        return getGrid(field, x, y);
    }

    /**
     * return grid from given board, row (x) and column (y).
     * 
     * @param board SOS
     * @param x     row index
     * @param y     column index
     * @return grid of correspond x and y
     */
    private static Grid getGrid(Grid[][] board, int x, int y) {
        return x < 0 || x >= board.length || y < 0 || y >= board[0].length ? null : board[x][y];
    }

    public Grid[][] getGrid() {
        return this.field;
    }

    public char getStats(int x, int y) {
        return getStats(field, x, y);
    }

    /**
     * get stats from grid from given x and y.
     * 
     * @param x for the index x
     * @param y for the index x
     */
    private static char getStats(Grid[][] board, int x, int y) {
        Grid grid = getGrid(board, x, y);
        return grid == null ? 'X' : grid.getStats();
    }

    /**
     * set char on a grid given x and y.
     * 
     * @param x for the index x
     * @param y for the index x
     * @param c set character
     */
    public long setGrid(int x, int y, char c) {
        this.field[x][y].setGrid(c);
        this.field[x][y].press();
        this.lastChar = c;
        return check(this.field, x, y);
    }

    /**
     * compute current board's state value given x and y.
     * 
     * @param x row index
     * @param y column index
     * @return long of current state value
     */
    public static long check(Grid[][] board, int x, int y) {
        long cnt = 0;
        if (getStats(board, x, y) == 'S') {
            if (checkLeftDiagonalDown(board, x, y)) {
                getGrid(board, x, y).strike(Strike.RIGHT_DIAGONAL);
                getGrid(board, x + 1, y - 1).strike(Strike.RIGHT_DIAGONAL);
                getGrid(board, x + 2, y - 2).strike(Strike.RIGHT_DIAGONAL);
                cnt = cnt + 1;
            }

            if (checkRightDiagonalDown(board, x, y)) {
                getGrid(board, x, y).strike(Strike.LEFT_DIAGONAL);
                getGrid(board, x + 1, y + 1).strike(Strike.LEFT_DIAGONAL);
                getGrid(board, x + 2, y + 2).strike(Strike.LEFT_DIAGONAL);
                cnt = cnt + 1;
            }

            if (checkLeftDiagonalUp(board, x, y)) {
                getGrid(board, x, y).strike(Strike.LEFT_DIAGONAL);
                getGrid(board, x - 1, y - 1).strike(Strike.LEFT_DIAGONAL);
                getGrid(board, x - 2, y - 2).strike(Strike.LEFT_DIAGONAL);
                cnt = cnt + 1;
            }

            if (checkRightDiagonalUp(board, x, y)) {
                getGrid(board, x, y).strike(Strike.RIGHT_DIAGONAL);
                getGrid(board, x - 1, y + 1).strike(Strike.RIGHT_DIAGONAL);
                getGrid(board, x - 2, y + 2).strike(Strike.RIGHT_DIAGONAL);
                cnt = cnt + 1;
            }

            if (checkVerticalUp(board, x, y)) {
                getGrid(board, x, y).strike(Strike.VERTICAL);
                getGrid(board, x - 1, y).strike(Strike.VERTICAL);
                getGrid(board, x - 2, y).strike(Strike.VERTICAL);
                cnt = cnt + 1;
            }

            if (checkVerticalDown(board, x, y)) {
                getGrid(board, x, y).strike(Strike.VERTICAL);
                getGrid(board, x + 1, y).strike(Strike.VERTICAL);
                getGrid(board, x + 2, y).strike(Strike.VERTICAL);
                cnt = cnt + 1;
            }

            if (checkLeftHorizontal(board, x, y)) {
                getGrid(board, x, y).strike(Strike.HORIZONTAL);
                getGrid(board, x, y - 1).strike(Strike.HORIZONTAL);
                getGrid(board, x, y - 2).strike(Strike.HORIZONTAL);
                cnt = cnt + 1;
            }

            if (checkRightHorizontal(board, x, y)) {
                getGrid(board, x, y).strike(Strike.HORIZONTAL);
                getGrid(board, x, y + 1).strike(Strike.HORIZONTAL);
                getGrid(board, x, y + 2).strike(Strike.HORIZONTAL);
                cnt = cnt + 1;
            }

        } else if (getStats(board, x, y) == 'O') {
            if (checkLeftDiagonalDown(board, x - 1, y + 1)) {
                getGrid(board, x - 1, y + 1).strike(Strike.RIGHT_DIAGONAL);
                getGrid(board, x, y).strike(Strike.RIGHT_DIAGONAL);
                getGrid(board, x + 1, y - 1).strike(Strike.RIGHT_DIAGONAL);
                cnt = cnt + 1;
            }

            if (checkRightDiagonalDown(board, x - 1, y - 1)) {
                getGrid(board, x - 1, y - 1).strike(Strike.LEFT_DIAGONAL);
                getGrid(board, x, y).strike(Strike.LEFT_DIAGONAL);
                getGrid(board, x + 1, y + 1).strike(Strike.LEFT_DIAGONAL);
                cnt = cnt + 1;
            }

            if (checkVerticalUp(board, x + 1, y)) {
                getGrid(board, x + 1, y).strike(Strike.VERTICAL);
                getGrid(board, x, y).strike(Strike.VERTICAL);
                getGrid(board, x - 1, y).strike(Strike.VERTICAL);
                cnt = cnt + 1;
            }

            if (checkLeftHorizontal(board, x, y + 1)) {
                getGrid(board, x, y + 1).strike(Strike.HORIZONTAL);
                getGrid(board, x, y).strike(Strike.HORIZONTAL);
                getGrid(board, x, y - 1).strike(Strike.HORIZONTAL);
                cnt = cnt + 1;
            }

        }
        cnt = cnt * cnt * cnt;
        return cnt;
    }

    private static boolean checkLeftDiagonalDown(Grid[][] board, int x, int y) {
        return getStats(board, x, y) == 'S'
            && getStats(board, x + 1, y - 1) == 'O'
            && getStats(board, x + 2, y - 2) == 'S';
    }

    private static boolean checkRightDiagonalDown(Grid[][] board, int x, int y) {
        return getStats(board, x, y) == 'S'
            && getStats(board, x + 1, y + 1) == 'O'
            && getStats(board, x + 2, y + 2) == 'S';
    }

    private static boolean checkLeftDiagonalUp(Grid[][] board, int x, int y) {
        return getStats(board, x, y) == 'S'
            && getStats(board, x - 1, y - 1) == 'O'
            && getStats(board, x - 2, y - 2) == 'S';
    }

    private static boolean checkRightDiagonalUp(Grid[][] board, int x, int y) {
        return getStats(board, x, y) == 'S'
            && getStats(board, x - 1, y + 1) == 'O'
            && getStats(board, x - 2, y + 2) == 'S';
    }

    private static boolean checkLeftHorizontal(Grid[][] board, int x, int y) {
        return getStats(board, x, y) == 'S'
            && getStats(board, x, y - 1) == 'O'
            && getStats(board, x, y - 2) == 'S';
    }

    private static boolean checkRightHorizontal(Grid[][] board, int x, int y) {
        return getStats(board, x, y) == 'S'
            && getStats(board, x, y + 1) == 'O'
            && getStats(board, x, y + 2) == 'S';
    }

    private static boolean checkVerticalUp(Grid[][] board, int x, int y) {
        return getStats(board, x, y) == 'S'
            && getStats(board, x - 1, y) == 'O'
            && getStats(board, x - 2, y) == 'S';
    }

    private static boolean checkVerticalDown(Grid[][] board, int x, int y) {
        return getStats(board, x, y) == 'S'
            && getStats(board, x + 1, y) == 'O'
            && getStats(board, x + 2, y) == 'S';
    }
}
