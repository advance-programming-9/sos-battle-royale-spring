package apps.game.core.arena;

public class GridRequest {
    private int pointX;
    private int pointY;
    private char character;

    public int getX() {
        return pointX;
    }

    public void setX(String x) {
        this.pointX = Integer.parseInt(x);
    }

    public int getY() {
        return pointY;
    }

    public void setY(String y) {
        this.pointY = Integer.parseInt(y);
    }

    public char getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character.charAt(0);
    }

}