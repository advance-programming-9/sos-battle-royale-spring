package apps.game.core.arena;

public enum Strike {
    HORIZONTAL,
    VERTICAL,
    LEFT_DIAGONAL,
    RIGHT_DIAGONAL
}
