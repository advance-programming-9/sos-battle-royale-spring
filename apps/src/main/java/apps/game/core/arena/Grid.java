package apps.game.core.arena;

public class Grid {
    private char stats;
    private boolean hasPressed;
    private boolean horizontalStrike;
    private boolean verticalStrike;
    private boolean leftDiagonalStrike;
    private boolean rightDiagonalStrike;

    /**
     * construct a grid.
     */
    public Grid() {
        this.stats = 'X';
        this.hasPressed = false;
        this.leftDiagonalStrike = false;
        this.rightDiagonalStrike = false;
        this.horizontalStrike = false;
        this.verticalStrike = false;
    }

    /**
     * set character S or O to a grid.
     * @param character set the character S or O
     */
    public void setGrid(char character) {
        if (!this.hasPressed) {
            this.stats = character;
            press();
        }
    }

    public void botSetGrid(char character) {
        this.stats = character;
    }

    public boolean getHasPressed() {
        return hasPressed;
    }

    public char getStats() {
        return this.stats;
    }

    public void press() {
        hasPressed = true;
    }

    /**
     * strike the grid by its strike as the direction.
     * @param strike case of the striking
     */
    public void strike(Strike strike) {
        switch (strike) {
            case HORIZONTAL:
                this.horizontalStrike = true;
                break;
            case VERTICAL:
                this.verticalStrike = true;
                break;
            case LEFT_DIAGONAL:
                this.leftDiagonalStrike = true;
                break;
            case RIGHT_DIAGONAL:
                this.rightDiagonalStrike = true;
                break;
            default:
                break;
        }
    }

    public boolean getLeftDiagonalStrike() {
        return this.leftDiagonalStrike;
    }

    public boolean getRightDiagonalStrike() {
        return this.rightDiagonalStrike;
    }

    public boolean getHorizontalStrike() {
        return this.horizontalStrike;
    }

    public boolean getVerticalStrike() {
        return this.verticalStrike;
    }
}