package apps.chat.controller;

import apps.chat.core.Message;
import apps.user.auth.JWT;
import apps.user.auth.JwtAuthorization;
import apps.user.model.User;
import apps.user.repository.MatchRepository;
import apps.user.repository.UserRepositoryJPA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class ChatSocketController {

    @Autowired
    private final MatchRepository matchRepository;

    @Autowired
    private final JWT jwt;

    @Autowired
    private final UserRepositoryJPA userRepositoryJPA;

    @Autowired
    private final SimpMessagingTemplate simpMessagingTemplate;

    /**
     * initiate the chat socket controller.
     * @param userRepositoryJPA for autowired
     * @param simpMessagingTemplate for autowired
     * @param matchRepository for autowired
     * @param jwt for autowired
     */
    public ChatSocketController(
        UserRepositoryJPA userRepositoryJPA,
        SimpMessagingTemplate simpMessagingTemplate,
        MatchRepository matchRepository,
        JWT jwt
    ) {
        this.userRepositoryJPA = userRepositoryJPA;
        this.matchRepository = matchRepository;
        this.jwt = jwt;
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    /**
     * send message endpoint.
     * @param message for autowired
     * @param roomId for autowired
     * @param authorization for autowired
     */
    @MessageMapping("/message/room/{roomId}")
    public void sendMessage(
        @RequestBody Message message,
        @DestinationVariable Integer roomId,
        @Header(value = "Authorization") String authorization
    ) {
        JwtAuthorization jwtAuthorization = new JwtAuthorization(jwt);
        jwtAuthorization.setAuthorization(authorization);
        if (jwtAuthorization.getError() != null) {
            return;
        }
        
        String username = jwtAuthorization.getUsername();

        User user = userRepositoryJPA.findByUsername(username).get();
        boolean isInThisMatch = matchRepository.isInThisMatch(user.getId(), roomId);
        if (isInThisMatch == false) {
            return;
        }

        simpMessagingTemplate.convertAndSend("/message/room/" + roomId, message);
    }
}