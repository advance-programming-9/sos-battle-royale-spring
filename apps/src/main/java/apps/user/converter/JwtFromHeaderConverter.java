package apps.user.converter;

import apps.user.auth.JWT;
import apps.user.auth.JwtAuthorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class JwtFromHeaderConverter implements Converter<String, JwtAuthorization> {

    @Autowired
    private JWT jwt;

    public JwtFromHeaderConverter(){
    }

    public JwtFromHeaderConverter(JWT jwt) {
        this.jwt = jwt;
    }

    @Override
    public JwtAuthorization convert(final String authorization) {

        final JwtAuthorization jwtAuthorization = new JwtAuthorization(jwt);
        jwtAuthorization.setAuthorization(authorization);

        return jwtAuthorization;
    }
}