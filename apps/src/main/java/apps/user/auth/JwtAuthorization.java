package apps.user.auth;

import apps.user.response.JwtResponse;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.security.SignatureException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class JwtAuthorization {
    private String jwtToken;
    private String authorization;
    private String error;
    private JwtResponse jwtResponse;

    @Autowired
    private final JWT jwt;

    public JwtAuthorization(JWT jwt) {
        this.jwt = jwt;
    }

    public String getAuthorization() {
        return authorization;
    }

    /**
     * set authorization from request header.
     * @param authorization given authorization from converter
     */
    public void setAuthorization(String authorization) {
        this.authorization = authorization;
        String[] tmpAuth = authorization.split(" ");
        if (tmpAuth.length != 2) {
            setError("INVALID_JWT");
            return;
        }

        String bearer = tmpAuth[0];
        String jwtToken = tmpAuth[1];

        if (!bearer.equals("Bearer")) {
            setError("INVALID_JWT");
            return;
        }

        boolean isValidated;
        try {
            isValidated = jwt.validateToken(jwtToken);
        } catch (SignatureException | ExpiredJwtException | MalformedJwtException e) {
            setError("INVALID_JWT");
            return;
        }

        if (!isValidated) {
            setError("INVALID_JWT");
            return;
        }
        
        setJwtToken(jwtToken);
    }

    /**
     * get absolute user's username from current token.
     * @return String username from current token
     */
    public String getUsername() {
        if (error != null) {
            return null;
        }
        String username = jwt.getUsernameFromToken(jwtToken);
        return username;
    }

    public String getJwtToken() {
        return jwtToken;
    }

    public String getError() {
        return error;
    }

    private void setJwtToken(String jwtToken) {
        this.jwtToken = jwtToken;
    }

    private void setError(String error) {
        this.error = error;
    }

    /**
     * jwt response instance for controller.
     * @return JwtResponse instance if the authorization is valid
     */
    public JwtResponse getJwtResponse() {
        if (error != null) {
            return null;
        }

        jwtResponse = new JwtResponse();

        String token = getJwtToken();
        String refreshedToken = jwt.refreshToken(token);

        jwtResponse.setJwtToken(refreshedToken);
        jwtResponse.setIsValidated(true);

        return jwtResponse;
    }
    
}