package apps.user.controller;

import apps.user.auth.JWT;
import apps.user.auth.JwtAuthorization;
import apps.user.model.User;
import apps.user.model.base.BaseUser;
import apps.user.model.base.BaseUserLogin;
import apps.user.model.validator.UserLoginWithValidator;
import apps.user.model.validator.UserRegisterWithValidator;
import apps.user.repository.MatchRepository;
import apps.user.repository.UserRepositoryJPA;
import apps.user.response.FetchMatchResponse;
import apps.user.response.JwtResponse;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
@CrossOrigin(origins = { "http://localhost:21483", "http://localhost:3000", "https://sos.ariqbasyar.id" })
public class UserController {

    @Autowired
    private final MatchRepository matchRepository;

    @Autowired
    private final JWT jwt;

    @Autowired
    private final UserRepositoryJPA userRepositoryJPA;

    /**
     * construct UserController.
     * @param userRepositoryJPA for autowired
     * @param matchRepository for autowired
     * @param jwt for autowired
     */
    public UserController(
        UserRepositoryJPA userRepositoryJPA,
        MatchRepository matchRepository,
        JWT jwt
    ) {
        this.jwt = jwt;
        this.userRepositoryJPA = userRepositoryJPA;
        this.matchRepository = matchRepository;
        setUp();
    }

    public void setUp() {
        UserLoginWithValidator.setRepository(userRepositoryJPA);
        UserRegisterWithValidator.setRepository(userRepositoryJPA);
    }

    /**
     * endpoint for client login.
     * @param validatedUser validated client input with repository JPA
     * @return BaseUserLogin for the login response
     */
    @PostMapping("/login")
    public ResponseEntity<BaseUserLogin> login(
        @RequestBody UserLoginWithValidator validatedUser
    ) {
        if (validatedUser.getIsValid()) {
            User absoluteUser = validatedUser.getUser();
            String username = absoluteUser.getUsername();

            String jwtToken = jwt.createToken(username);
            validatedUser.setJwtToken(jwtToken);
            return ResponseEntity.ok(validatedUser);
        }
        return ResponseEntity.badRequest().body(validatedUser);
    }

    /**
     * endpoint for client register.
     * @param validatedUser validated user from client input
     * @return BaseUser for the register response
     */
    @PostMapping("/register")
    public ResponseEntity<BaseUser> register(
        @RequestBody UserRegisterWithValidator validatedUser
    ) {
        boolean isValidated;
        isValidated = validatedUser.getIsValid();
        if (isValidated) {
            User absoluteUser = validatedUser.getUser();
            userRepositoryJPA.save(absoluteUser);
            return ResponseEntity.ok(validatedUser);
        }
        return ResponseEntity.badRequest().body(validatedUser);
    }

    /**
     * validate the jwt.
     * @param jwtAuthorization authorizarion from header
     * @return
     */
    @PostMapping("/jwt/validate")
    public ResponseEntity<JwtResponse> jwtValidate(
        @RequestHeader(name = "Authorization") JwtAuthorization jwtAuthorization
    ) {
        String error = jwtAuthorization.getError();
        if (error != null) {
            return ResponseEntity.badRequest().body(new JwtResponse());
        }

        JwtResponse jwtResponse = jwtAuthorization.getJwtResponse();
        return ResponseEntity.ok(jwtResponse);
    }

    /**
     * fetch current match from client's jwt token.
     * @param jwtAuthorization from request header
     */
    @GetMapping("/match")
    public ResponseEntity<FetchMatchResponse> fetchMatchByJwt(
        @RequestHeader(name = "Authorization") JwtAuthorization jwtAuthorization
    ) {
        FetchMatchResponse fetchMatchResponse = new FetchMatchResponse();

        String error = jwtAuthorization.getError();
        if (error != null) {
            return ResponseEntity.badRequest().body(fetchMatchResponse);
        }

        String username = jwtAuthorization.getUsername();
        Optional<User> optionalUser = userRepositoryJPA.findByUsername(username);
        if (!optionalUser.isPresent()) {
            return ResponseEntity.badRequest().body(fetchMatchResponse);
        }

        User absoluteUser = optionalUser.get();
        Integer roomId = matchRepository.getMatchId(absoluteUser.getId());

        fetchMatchResponse.setRoomId(roomId == null ? -1 : roomId);
        fetchMatchResponse.setUsername(username);

        return ResponseEntity.ok(fetchMatchResponse);
    }
}