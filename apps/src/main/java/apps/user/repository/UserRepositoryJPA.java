package apps.user.repository;

import apps.user.model.User;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepositoryJPA extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);

    Optional<User> findByEmail(String email);

    List<User> findByOrderByScoreDesc();

    @Query(nativeQuery = true, value = 
        "SELECT * FROM tugaskelompok.user ORDER BY score DESC LIMIT 10"
    )
    List<User> findByOrderByScoreDescLimit10();

    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = 
        "UPDATE tugaskelompok.user SET score = :score WHERE id = :id"
    )
    void updateScoreById(@Param("id") long id,@Param("score") long score);
}
