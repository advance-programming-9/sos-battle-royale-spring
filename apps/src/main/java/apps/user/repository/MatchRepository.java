package apps.user.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;

@Repository
public class MatchRepository {

    /**
     * every user id, match one to one to 
     * match id or room id or game id.
     */
    private Map<Long, Integer> matches = new HashMap<>();
    private Map<Long, Long> inGameScores = new HashMap<>();

    public List<Integer> getMatches() {
        return new ArrayList<>(matches.values());
    }

    public List<Long> getInGameScores() {
        return new ArrayList<>(inGameScores.values());
    }

    public void addInGameScore(Long userId, Long adderInGameScore) {
        Long currentInGameScore = inGameScores.get(userId);
        inGameScores.put(userId, currentInGameScore + adderInGameScore);
    }
    
    public void setInGameScore(Long userId, Long inGameScore) {
        inGameScores.put(userId, inGameScore);
    }

    public Long getInGameScore(Long userId) {
        return inGameScores.get(userId);
    }

    public Integer getMatchId(Long userId) {
        return matches.get(userId);
    }

    /**
     * set match for specific user.
     * @param userId user id.
     * @param roomId room id.
     */
    public void setMatch(Long userId, Integer roomId) {
        matches.put(userId, roomId);

        Long currentScore = inGameScores.get(userId);
        if (currentScore != null) {
            setInGameScore(userId, currentScore);
        } else {
            setInGameScore(userId, 0L);
        }
    }

    public void removeMatchId(Long userId) {
        matches.remove(userId);
        inGameScores.remove(userId);
    }

    public boolean isInAMatch(Long userId) {
        return matches.get(userId) != null;
    }

    /**
     * check if user is in that match.
     * @param userId user id
     * @param roomId match/room id
     * @return boolean true if the user is on the match with same roomId
     */
    public boolean isInThisMatch(Long userId, Integer roomId) {
        Integer actualMatchId = matches.get(userId);
        if (actualMatchId == null) {
            return false;
        }
        return actualMatchId.equals(roomId);
    }
}
