package apps.user.model.validator.atomic;

import java.util.regex.Pattern;

public class EmailValidator {
    private static final Pattern emailPattern =
        Pattern.compile("^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$");

    /**
     * validate an email with length constraint and regex pattern.
     * @param email from client input
     * @return boolean when it's valid/match email pattern
     */
    public static boolean validate(String email) {
        if (email.length() < 3 || email.length() > 100) {
            return false;
        }
        if (!emailPattern.matcher(email).find()) {
            return false;
        }
        return true;
    }
}