package apps.user.model.validator.atomic;

import java.util.regex.Pattern;

public class UsernameValidator {
    private static final Pattern usernamePattern =
        Pattern.compile("^[a-zA-Z0-9_]{3,15}$");

    private static final Pattern botPattern =
        Pattern.compile("^\\[BOT\\]");

    /**
     * username validator with length and regex pattern constraint.
     * @param username from client input
     * @return boolean when it's match the username pattern
     */
    public static boolean validate(String username) {
        return usernamePattern.matcher(username).find()
            && !isBotUsername(username);
    }

    /**
     * is bot's username.
     * @param username from client input
     * @return boolean 
     */
    public static boolean isBotUsername(String username) {
        return botPattern.matcher(username).find();
    }
}