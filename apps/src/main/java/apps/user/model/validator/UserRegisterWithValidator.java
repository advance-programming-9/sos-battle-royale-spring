package apps.user.model.validator;

import apps.user.model.User;
import apps.user.model.base.BaseUser;
import apps.user.model.validator.atomic.EmailValidator;
import apps.user.model.validator.atomic.PasswordValidator;
import apps.user.model.validator.atomic.UsernameValidator;
import apps.user.repository.UserRepositoryJPA;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.crypto.bcrypt.BCrypt;

public class UserRegisterWithValidator implements BaseUser {
    private String emailError;
    private String usernameError;
    private String passwordError;
    private String password;

    private static UserRepositoryJPA userRepositoryJPA;

    private User user;
    private boolean isValid = true;

    public UserRegisterWithValidator() {
        this.user = new User();
    }

    @Override
    public void setUsername(String username) {
        boolean isUsernameValid = UsernameValidator.validate(username);
        if (isUsernameValid) {
            if (userRepositoryJPA.findByUsername(username).isPresent()) {
                this.isValid = false;
                this.usernameError = "This username already exists, please choose another username";
            } else {
                user.setUsername(username);
            }
        } else {
            this.isValid = false;
            this.usernameError = "Invalid username, make sure the length is at least 3 "
                + "and only contains alphanumeric";
        }
    }

    @Override
    public void setEmail(String email) {
        boolean isEmailValid = EmailValidator.validate(email);
        if (isEmailValid) {
            if (userRepositoryJPA.findByEmail(email).isPresent()) {
                this.isValid = false;
                this.emailError = "This email already exists, please choose another email";
            } else {
                user.setEmail(email);
            }
        } else {
            this.isValid = false;
            this.emailError = "Invalid email, please enter a valid email";
        }
    }

    @Override
    public void setPassword(String password) {
        boolean isPasswordValid = PasswordValidator.validate(password);
        this.password = password;
        if (isPasswordValid) {
            user.setPassword(BCrypt.hashpw(password, BCrypt.gensalt(4)));
        } else {
            this.isValid = false;
            this.passwordError = "Invalid password, make sure it consists of at least 3 characters"
                + " and max 100 characters";
        }
    }

    /**
     * set second password make sure it's equal first password.
     * @param password2 from client input.
     */
    public void setPassword2(String password2) {
        if (!this.password.equals(password2)) {
            this.isValid = false;
            this.passwordError = "Invalid password, make sure you repeat the same password";
        }
    }

    public String getEmailError() {
        return emailError;
    }

    public String getUsernameError() {
        return usernameError;
    }

    public String getPasswordError() {
        return passwordError;
    }

    public boolean getIsValid() {
        return isValid;
    }

    /**
     * allows to get the absolute user after validated.
     * @return User when it's validated.
     */
    @JsonIgnore
    public User getUser() {
        if (this.isValid == true) {
            return user;
        }
        return null;
    }

    public static void setRepository(UserRepositoryJPA userRepositoryJPA) {
        UserRegisterWithValidator.userRepositoryJPA = userRepositoryJPA;
    }
}