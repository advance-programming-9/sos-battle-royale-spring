package apps.user.model.validator.atomic;

public class PasswordValidator {

    /**
     * validate password with length constraint.
     * @param password from client input
     * @return boolean when it's valid
     */
    public static boolean validate(String password) {
        if (password.length() < 3 || password.length() > 100) {
            return false;
        }
        return true;
    }
}