package apps.user.model.validator;

import apps.user.model.User;
import apps.user.model.base.BaseUserLogin;
import apps.user.model.validator.atomic.PasswordValidator;
import apps.user.model.validator.atomic.UsernameValidator;
import apps.user.repository.UserRepositoryJPA;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Optional;
import org.springframework.security.crypto.bcrypt.BCrypt;

public class UserLoginWithValidator implements BaseUserLogin {
    private static UserRepositoryJPA userRepositoryJPA;

    private String usernameError;
    private String passwordError;
    private String invalidLogin;

    private String tempUsername;
    private String tempPassword;

    private String jwtToken;
    
    private User user;
    private boolean isValid = false;

    @Override
    public void setUsername(String username) {
        boolean isUsernameValid = UsernameValidator.validate(username);
        if (isUsernameValid) {
            this.tempUsername = username;
            this.isValid = true;
            validateWithRepository();
        } else {
            this.usernameError = "Invalid username, make sure the length is at least 3 "
                + "and only contains alphanumeric";
        }
    }

    @Override
    public void setPassword(String password) {
        boolean isPasswordValid = PasswordValidator.validate(password);
        if (isPasswordValid) {
            this.tempPassword = password;
            this.isValid = true;
            validateWithRepository();
        } else {
            this.passwordError = "Invalid password, make sure it consists of at least 3 characters";
        }
    }

    /**
     * validate user input with repository.
     */
    public void validateWithRepository() {
        if (this.tempUsername == null || this.tempPassword == null) {
            return;
        }
        Optional<User> tempOptionalUser = userRepositoryJPA.findByUsername(tempUsername);
        if (!tempOptionalUser.isPresent()) {
            invalidLogin = "Incorrect username or password";
            isValid = false;
            return;
        }
        User tempUser = tempOptionalUser.get();
        boolean passwordValid = BCrypt.checkpw(this.tempPassword, tempUser.getPassword());
        if (!passwordValid) {
            invalidLogin = "Incorrect username or password";
            isValid = false;
            return;
        }
        isValid = true;
        this.user = tempUser;
    }

    public String getUsernameError() {
        return usernameError;
    }

    public String getPasswordError() {
        return passwordError;
    }

    public String getInvalidLogin() {
        return invalidLogin;
    }

    public boolean getIsValid() {
        return isValid;
    }

    public String getJwtToken() {
        return jwtToken;
    }

    /**
     * get the user after validated with repository.
     * @return User when it's valid or pass the validation
     */
    @JsonIgnore
    public User getUser() {
        if (this.isValid) {
            return user;
        }
        return null;
    }

    public void setJwtToken(String jwtToken) {
        this.jwtToken = jwtToken;
    }

    public static void setRepository(UserRepositoryJPA userRepositoryJPA) {
        UserLoginWithValidator.userRepositoryJPA = userRepositoryJPA;
    }
}