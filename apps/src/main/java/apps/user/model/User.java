package apps.user.model;

import apps.user.model.base.BaseUser;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import org.hibernate.validator.constraints.Length;

@Entity
@Table(name = "user")
public class User implements BaseUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
        })
    private Set<User> friends = new HashSet<>();

    @Length(min = 3, max = 15)
    @Column(nullable = false, unique = true)
    private String username;

    @Length(min = 3, max = 100)
    @Column(nullable = false, unique = true)
    private String email;

    @Length(min = 3, max = 100)
    @Column(nullable = false)
    private String password;

    @Column(nullable = true)
    private long score;
    
    public User() {

    }

    public User(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getScore() {
        return score;
    }

    public void setScore(long score) {
        this.score = score;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void addFriend(User friend) {
        this.friends.add(friend);
    }

    public Set<User> getFriends() {
        return friends;
    }

    public void deleteFriend(User friend) {
        this.friends.remove(friend);
    }
}
