package apps.user.model.base;

public interface BaseUser {

    public void setUsername(String username);

    public void setEmail(String email);
    
    public void setPassword(String password);

}