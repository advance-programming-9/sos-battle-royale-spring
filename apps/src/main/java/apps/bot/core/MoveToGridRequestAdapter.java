package apps.bot.core;

import apps.game.core.arena.GridRequest;

public class MoveToGridRequestAdapter {
    private Move move;

    public MoveToGridRequestAdapter(Move move) {
        this.move = move;
    }    

    /**
     * convert Move to GridRequest.
     * @return GridRequest corresponded to Move
     */
    public GridRequest convert() {
        if (move == null) {
            return null;
        }
        GridRequest gridRequest = new GridRequest();
        gridRequest.setX(String.valueOf(move.getX()));
        gridRequest.setY(String.valueOf(move.getY()));
        gridRequest.setCharacter(String.valueOf(move.getCharacter()));
        return gridRequest;
    }
}