package apps.bot.core;

public class Move {
    private int indexX;
    private int indexY;
    private char character;

    public int getX() {
        return indexX;
    }

    public void setX(int indexX) {
        this.indexX = indexX;
    }

    public int getY() {
        return indexY;
    }

    public void setY(int indexY) {
        this.indexY = indexY;
    }

    public char getCharacter() {
        return character;
    }

    public void setCharacter(char character) {
        this.character = character;
    }
}