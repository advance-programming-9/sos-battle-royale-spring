package apps.bot.core;

import apps.game.core.arena.Arena;
import apps.game.core.arena.Grid;
import apps.game.core.arena.GridRequest;
import java.util.Random;

public class Bot {
    private Long id;
    private String username;
    private static final int MAX_DEPTH = 1;

    public Bot() {
        this("[BOT]");
    }

    public Bot(String username) {
        this((long) (new Random()).nextInt((int) 1e3 + (int) 1e6) - (int) 1e6, username);
    }

    public Bot(Long id, String username) {
        this.id = id;
        this.username = username + String.valueOf(Math.abs(this.id));
    }

    public Long getId() {
        return this.id;
    }

    public String getUsername() {
        return this.username;
    }

    /**
     * minimax algorithm to find bot's best move.
     * 
     * @param board    board
     * @param moveChar 'S' or 'O'
     * @param x        row index
     * @param y        column index
     * @param depth    depth of the minimax find. For the 10x10 board it's best to
     *                 set depth to 2 (ones indexing)
     * @param isMax    maximize or minimizing current move
     * @return long score from the current move
     */
    public long minimax(Grid[][] board, char moveChar, int x, int y, int depth, boolean isMax) {
        int height = board.length;
        int width = board[0].length;

        if (depth == MAX_DEPTH) {
            return -Arena.check(board, x, y);
        }

        if (isMax) {
            long best = Long.MIN_VALUE;

            // Traverse all cells
            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    // Check if cell is empty
                    if (board[i][j].getStats() == 'X') {
                        // Make the move
                        board[i][j].botSetGrid(moveChar);

                        // Call minimax recursively and choose
                        // the maximum value
                        long val = minimax(
                            board,
                            moveChar == 'S' ? 'O' : 'S', i, j,
                            depth + 1,
                            false
                        );
                        best = Math.max(best, val);

                        // Undo the move
                        board[i][j].botSetGrid('X');
                    }
                }
            }
            return best;
        } else { // If this minimizer's move
            long best = Long.MAX_VALUE;

            // Traverse all cells
            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    // Check if cell is empty
                    if (board[i][j].getStats() == 'X') {
                        // Make the move
                        board[i][j].botSetGrid(moveChar);

                        // Call minimax recursively and choose
                        // the minimum value
                        long val = minimax(
                            board,
                            moveChar == 'S' ? 'O' : 'S', i, j,
                            depth + 1,
                            true
                        );
                        best = Math.min(best, val);

                        // Undo the move
                        board[i][j].botSetGrid('X');
                    }
                }
            }
            return best;
        }
    }

    /**
     * find best move given arena.
     * @param arena arena of the board
     * @return grid with x and y value with the best state value
     */
    public GridRequest findBestMove(Arena arena) {
        long bestVal = Long.MIN_VALUE;
        Move bestMove = new Move();
        Grid[][] originalBoard = arena.getGrid();
        Grid[][] board = new Grid[originalBoard.length][originalBoard[0].length];
        for (int i = 0; i < originalBoard.length; i++) {
            for (int j = 0; j < originalBoard[i].length; j++) {
                board[i][j] = new Grid();
                board[i][j].botSetGrid(originalBoard[i][j].getStats());
            }
        }
        bestMove.setX(-1);
        bestMove.setY(-1);
        int height = arena.getHeight();
        int width = arena.getWidth();
        char moveChar = arena.getLastChar() == 'S' ? 'O' : 'S';

        int i = 0;
        int j = 0;
        for (i = 0; i < height; i++) {
            for (j = 0; j < width; j++) {
                if (board[i][j].getStats() == 'X') {
                    board[i][j].botSetGrid(moveChar);

                    long moveVal = minimax(
                        board,
                        moveChar == 'S' ? 'O' : 'S', i, j,
                        0,
                        false
                    );

                    board[i][j].botSetGrid('X');

                    if (moveVal > bestVal) {
                        bestMove.setX(i);
                        bestMove.setY(j);
                        bestVal = moveVal;
                    }
                }
            }
        }
        System.out.println(bestVal);
        if (bestMove.getX() == -1 || bestMove.getY() == -1) {
            bestMove.setX(i);
            bestMove.setY(j);
        }
        bestMove.setCharacter(moveChar);

        MoveToGridRequestAdapter converter = new MoveToGridRequestAdapter(bestMove);
        GridRequest gridRequest = converter.convert();

        board = null;
        System.gc();

        return gridRequest;
    }
}
