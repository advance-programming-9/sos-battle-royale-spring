package apps.bot.repository;

import apps.bot.core.Bot;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;

@Repository
public class BotRepository {

    private Map<Long, Bot> botRepository = new HashMap<>();

    public List<Bot> getBots() {
        return new ArrayList<>(botRepository.values());
    }

    public void enterBot(Bot bot) {
        botRepository.put(bot.getId(), bot);
    }

    public Bot getBotById(Long botId) {
        return botRepository.get(botId);
    }

    public void deleteBotById(Long botId) {
        botRepository.remove(botId);
    }
}