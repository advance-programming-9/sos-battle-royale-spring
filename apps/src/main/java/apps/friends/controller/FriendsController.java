package apps.friends.controller;

import apps.friends.core.FriendState;
import apps.friends.core.FriendsRequest;
import apps.friends.response.FindResponse;
import apps.friends.response.FriendsResponse;
import apps.room.core.RoomResponse;
import apps.room.core.base.SecuredRoom;
import apps.room.repository.RoomRepository;
import apps.user.auth.JwtAuthorization;
import apps.user.model.User;
import apps.user.repository.MatchRepository;
import apps.user.repository.UserRepositoryJPA;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/friends")
@CrossOrigin(origins = { "http://localhost:21483", "http://localhost:3000", "https://sos.ariqbasyar.id" })
public class FriendsController {

    @Autowired
    private final UserRepositoryJPA userRepositoryJPA;

    @Autowired
    private final MatchRepository matchRepository;

    @Autowired
    private final RoomRepository roomRepository;

    /**
     * construct UserController.
     * @param userRepositoryJPA for autowired
     */
    public FriendsController(
        UserRepositoryJPA userRepositoryJPA,
        MatchRepository matchRepository,
        RoomRepository roomRepository
    ) {
        this.userRepositoryJPA = userRepositoryJPA;
        this.matchRepository = matchRepository;
        this.roomRepository = roomRepository;
    }

    /**
     * fetch friend from client's jwt token.
     * @param jwtAuthorization from request header
     */
    @GetMapping("/fetch")
    public ResponseEntity<FriendsResponse> fetchUserFriend(
        @RequestHeader(name = "Authorization") JwtAuthorization jwtAuthorization
    ) {
        FriendsResponse friendResponse = new FriendsResponse();

        String error = jwtAuthorization.getError();
        if (error != null) {
            friendResponse.setError("Authorization Error!");
            return ResponseEntity.badRequest().body(friendResponse);
        }

        String username = jwtAuthorization.getUsername();
        Optional<User> optionalUser = userRepositoryJPA.findByUsername(username);
        if (!optionalUser.isPresent()) {
            friendResponse.setError("Username invalid");
            return ResponseEntity.badRequest().body(friendResponse);
        }

        User absoluteUser = optionalUser.get();
        Set<User> friends = absoluteUser.getFriends();
        Set<FriendState> friendStates = new HashSet<>();
        for (User friend: friends) {
            FriendState friendState = new FriendState();
            String friendName = friend.getUsername();
            long id = friend.getId();
            if (matchRepository.getMatchId(id) != null) {
                int roomId = matchRepository.getMatchId(id);
                friendState.setRoomId(roomId);
                if (roomRepository.getRoomById(roomId) != null) {
                    SecuredRoom securedRoom = roomRepository.getRoomById(roomId);
                    RoomResponse roomResponse = securedRoom.getRoomResponse();
                    boolean isProtected = securedRoom.getIsProtected();
                    String state = roomResponse.getState();
                    friendState.setIsProtected(isProtected);
                    friendState.setState(state);
                }
            }
            friendState.setName(friendName);
            friendState.setId(id);
            friendState.setScore(friend.getScore());
            friendStates.add(friendState);
        }
        friendResponse.setFriendState(friendStates);

        return ResponseEntity.ok(friendResponse);
    }

    /**
     * find player in friends from client's jwt token.
     * @param jwtAuthorization from request header
     * @param friendRequest from request body
     */
    @PostMapping("/find")
    public ResponseEntity findUser(
        @RequestHeader(name = "Authorization") JwtAuthorization jwtAuthorization,
        @RequestBody FriendsRequest friendRequest
    ) {
        FriendsResponse friendResponse = new FriendsResponse();

        String error = jwtAuthorization.getError();
        if (error != null) {
            friendResponse.setError("Authorization Error!");
            return ResponseEntity.badRequest().body(friendResponse);
        }

        String username = jwtAuthorization.getUsername();
        Optional<User> optionalUser = userRepositoryJPA.findByUsername(username);
        if (!optionalUser.isPresent()) {
            friendResponse.setError("Username invalid");
            return ResponseEntity.badRequest().body(friendResponse);
        }

        String playerFoundUsername = friendRequest.getFriendUsername();
        Optional<User> optionalUserFriend = userRepositoryJPA.findByUsername(playerFoundUsername);
        if (!optionalUserFriend.isPresent()) {
            friendResponse.setError("Player not found!");
            return ResponseEntity.badRequest().body(friendResponse);
        }

        User absoluteFriend = optionalUserFriend.get();
        FindResponse findResponse = new FindResponse();
        findResponse.setFriend(absoluteFriend);

        return ResponseEntity.ok(findResponse);
    }

    /**
     * add player in friends from client's jwt token.
     * @param jwtAuthorization from request header
     * @param friendRequest from request body
     */
    @PutMapping("/add")
    public ResponseEntity addFriend(
            @RequestHeader(name = "Authorization") JwtAuthorization jwtAuthorization,
            @RequestBody FriendsRequest friendRequest
    ) {
        FriendsResponse friendResponse = new FriendsResponse();

        String error = jwtAuthorization.getError();
        if (error != null) {
            friendResponse.setError("Authorization Error!");
            return ResponseEntity.badRequest().body(friendResponse);
        }

        String username = jwtAuthorization.getUsername();
        Optional<User> optionalUser = userRepositoryJPA.findByUsername(username);
        if (!optionalUser.isPresent()) {
            friendResponse.setError("Username invalid");
            return ResponseEntity.badRequest().body(friendResponse);
        }

        String playerFoundUsername = friendRequest.getFriendUsername();
        Optional<User> optionalUserFriend = userRepositoryJPA.findByUsername(playerFoundUsername);
        if (!optionalUserFriend.isPresent()) {
            friendResponse.setError("Player not found!");
            return ResponseEntity.badRequest().body(friendResponse);
        }

        User absoluteUser = optionalUser.get();
        User absoluteFriend = optionalUserFriend.get();
        absoluteUser.addFriend(absoluteFriend);
        absoluteFriend.addFriend(absoluteUser);

        userRepositoryJPA.save(absoluteUser);
        userRepositoryJPA.save(absoluteFriend);

        FindResponse findResponse = new FindResponse();
        findResponse.setFriend(absoluteFriend);

        return ResponseEntity.ok(findResponse);
    }
}