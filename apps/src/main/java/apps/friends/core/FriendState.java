package apps.friends.core;

public class FriendState {
    private String name;
    private long id;
    private int roomId;
    private String state;
    private long score;
    private boolean isProtected;

    /**
     * Constructor FriendState where no input.
     */
    public FriendState() {
        this.name = "dummy";
        this.id = 100;
        this.state = "-";
    }

    /**
     * Constructor FriendState where there is name,id,room and state.
     */
    public FriendState(String name,long id, int roomId,String state) {
        this.name = name;
        this.id = id;
        this.roomId = roomId;
        this.state = state;
    }

    /**
     * get name.
     * @return String of name
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * get id.
     * @return long of id
     */
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    /**
     * get room.
     * @return int of id
     */
    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    /**
     * get state.
     * @return String of state
     */
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setScore(long score) {
        this.score = score;
    }

    public long getScore() {
        return this.score;
    }

    public void setIsProtected(boolean isProtected) {
        this.isProtected = isProtected;
    }

    public boolean getIsProtected() {
        return this.isProtected;
    }
}
