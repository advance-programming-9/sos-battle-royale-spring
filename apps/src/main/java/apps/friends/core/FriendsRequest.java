package apps.friends.core;

public class FriendsRequest {

    private String friendUsername;

    public void setFriendUsername(String friendUsername) {
        this.friendUsername = friendUsername;
    }

    public String getFriendUsername() {
        return this.friendUsername;
    }
}
