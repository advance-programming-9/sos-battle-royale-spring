package apps.friends.core;

import java.util.ArrayList;

public class Friends {

    private Long idUser;
    private ArrayList<Long> friendsIdUser;

    public Friends(Long idUser) {
        friendsIdUser = new ArrayList<>();
        this.idUser = idUser;
    }

    public void addFriend(Long idFriend) {
        friendsIdUser.add(idFriend);
    }

    public void deleteFriend(Long idFriend) {
        friendsIdUser.remove(idFriend);
    }

    public ArrayList<Long> getFriends() {
        return friendsIdUser;
    }

    public long getUserId() {
        return idUser;
    }

}