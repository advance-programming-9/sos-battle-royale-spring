package apps.friends.response;

import apps.user.model.User;

public class FindResponse {
    private User friend;

    public void setFriend(User friend) {
        this.friend = friend;
    }

    public String getUsername() {
        return friend.getUsername();
    }

    public long getScore() {
        return friend.getScore();
    }
}