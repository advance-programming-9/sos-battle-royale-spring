package apps.friends.response;

import apps.friends.core.FriendState;
import apps.scoreboard.core.adapter.SafeUser;
import apps.user.model.User;
import java.util.HashSet;
import java.util.Set;

public class FriendsResponse {
    private Set<User> friend = new HashSet<>();
    private String error;
    private String username;
    private Set<FriendState> friendState = new HashSet<>();

    public void setFriend(Set<User> friend) {
        this.friend = friend;
    }

    public void setFriendState(Set<FriendState> friendState) {
        this.friendState = friendState;
    }
    
    /**
     * get friends.
     * @return Set of friends
     */
    public Set<SafeUser> getFriend() {
        Set<SafeUser> friends = new HashSet<>();
        for (User user: friend) {
            friends.add(new SafeUser(user));
        }
        return friends;
    }

    /**
     * get friends with state.
     * @return Set of friends with state
     */
    public Set<FriendState> getFriendState() {
        return friendState;
    }

    public String getError() {
        return this.error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return this.username;
    }
}
