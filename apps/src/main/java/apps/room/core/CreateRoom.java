package apps.room.core;

import apps.room.core.base.SecuredRoom;

public class CreateRoom {
    private String title;
    private String password;
    private SecuredRoom securedRoom;

    /**
     * set the room title.
     * @param title for the room
     */
    public void setTitle(String title) {
        if (title == null || title.equals("")) {
            this.title = "default";
        } else {
            this.title = title;
        }
    }

    /**
     * set room password.
     * @param password for the room
     */
    public void setPassword(String password) {
        if (password == null || password.equals("")) {
            this.password = "";
        } else {
            this.password = password;
        }
    }

    /**
     * create the room after set title and password.
     */
    public void create() {
        Room room = new Room(title);
        if (password.equals("")) {
            securedRoom = new RoomWithoutPassword(room);
        } else {
            securedRoom = new RoomWithPassword(room, password);
        }
    }

    public SecuredRoom getRoom() {
        return securedRoom;
    }

    public String getPassword() {
        return password;
    }
}