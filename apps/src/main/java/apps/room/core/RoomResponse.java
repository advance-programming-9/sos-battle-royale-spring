package apps.room.core;

import apps.bot.core.Bot;
import apps.bot.repository.BotRepository;
import apps.game.core.arena.Arena;
import apps.scoreboard.core.adapter.SafeUser;
import apps.user.model.User;
import apps.user.repository.MatchRepository;
import apps.user.repository.UserRepositoryJPA;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class RoomResponse {

    private String error;
    private Room room = new Room();

    private static UserRepositoryJPA userRepositoryJPA;
    private static MatchRepository matchRepository;
    private static BotRepository botRepository;

    public RoomResponse() {

    }

    /**
     * set repository for response.
     * @param userRepositoryJPA user repository
     * @param matchRepository match repostory
     * @param botRepository bot repository
     */
    public static void setUserRepositoryJPA(
        UserRepositoryJPA userRepositoryJPA,
        MatchRepository matchRepository,
        BotRepository botRepository
    ) {
        RoomResponse.userRepositoryJPA = userRepositoryJPA;
        RoomResponse.matchRepository = matchRepository;
        RoomResponse.botRepository = botRepository;
    }

    public RoomResponse(String error) {
        this.error = error;
    }
    
    public RoomResponse(Room room) {
        this.room = room;
    }

    public Arena getArena() {
        return this.room.getArena();
    }

    /**
     * decide who is the room master.
     * @return username of the room master
     */
    public String getRoomMaster() {
        long idRoomMaster = room.getRoomMaster();
        if (idRoomMaster < 1) {
            return null;
        }
        Optional<User> search = userRepositoryJPA.findById(idRoomMaster);
        User roomMaster = search.get();

        return roomMaster.getUsername();
    }
    
    public int getCountPeople() {
        return this.room.getCountPeople();
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getState() {
        return room.getState();
    }

    /**
     * decide who is the next turn.
     * @return username who is the next turn.
     */
    public String getWhoIsNext() {
        long userId = room.getWhoIsNext();
        if (userId == -1) {
            return null;
        }
        if (userId < -1) {
            Bot bot = botRepository.getBotById(userId);
            return bot.getUsername();
        }
        User user = userRepositoryJPA.findById(userId).get();
        return user.getUsername();
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    /**
     * get all users from the room with a hidden password.
     * @return safe users with a hidden password
     */
    public List<SafeUser> getUsers() {
        List<SafeUser> safeUsers = new ArrayList<>();
        List<Long> usersId = room.getUsers();

        for (Long userId: usersId) {
            SafeUser safeUser;
            if (userId < -1) {
                Bot bot = botRepository.getBotById(userId);
                safeUser = new SafeUser(bot);
            } else {
                User user = userRepositoryJPA.findById(userId).get();
                safeUser = new SafeUser(user);
            }
            Long inGameScore = matchRepository.getInGameScore(userId);
            // LoggerFactory.getLogger(RoomResponse.class).info(String.valueOf(inGameScore));
            safeUser.setInGameScore(inGameScore);
            safeUsers.add(safeUser);
        }

        return safeUsers;
    }

    /**
     * get sorted users from the room with a hidden password.
     * @return safe user with a hidden password
     */
    public List<SafeUser> getSortedUsersByInGameScore() {
        List<SafeUser> safeUsers = getUsers();
        Collections.sort(safeUsers);
        return safeUsers;
    }
}