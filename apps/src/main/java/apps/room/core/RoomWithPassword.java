package apps.room.core;

import apps.bot.core.Bot;
import apps.game.core.arena.GridRequest;
import apps.room.core.base.SecuredRoom;
import apps.user.model.User;
import java.util.List;
import org.springframework.security.crypto.bcrypt.BCrypt;

public class RoomWithPassword implements SecuredRoom {
    private Room room;
    private String password;
    private RoomResponse roomResp;

    public RoomWithPassword(Room room, String password) {
        this.room = room;
        this.password = BCrypt.hashpw(password, BCrypt.gensalt(4));
    }

    @Override
    public boolean getIsProtected() {
        return true;
    }

    @Override
    public Room enter(User user, String password) {
        if (isInThisRoom(user.getId())) {
            return room.enter(user);
        }
        boolean isMatch = BCrypt.checkpw(password, this.password);
        if (isMatch) {
            return room.enter(user);
        }
        return null;
    }

    @Override
    public Room enter(User user) throws Exception {
        throw new Exception("invalid, use enter(User user, String password) instead!");
    }

    @Override
    public int getRoomId() {
        return room.getRoomId();
    }

    @Override
    public String getTitle() {
        return room.getTitle();
    }

    @Override
    public int getCountPeople() {
        return room.getCountPeople();
    }

    @Override
    public boolean quit(User user) {
        return room.quit(user);
    }

    @Override
    public RoomResponse getRoomResponse() {
        if (roomResp == null) {
            roomResp = new RoomResponse(room);
        }
        return roomResp;
    }

    @Override
    public boolean start() {
        return room.start();
    }

    @Override
    public long click(GridRequest gridRequest) {
        return room.click(gridRequest);
    }

    @Override
    public void passTurn(long userId) {
        room.passTurn(userId);
    }

    private boolean isInThisRoom(long userId) {
        return room.getUsers().indexOf(userId) != -1;
    }

    @Override
    public boolean enterBot(User user, Bot bot) {
        if (room.getRoomMaster() != user.getId()) {
            return false;
        }
        return room.enterBot(bot);
    }

    @Override
    public long getIdWhoIsNext() {
        return room.getWhoIsNext();
    }

    @Override
    public Long deleteBot(User user) {
        if (room.getRoomMaster() != user.getId()) {
            return -1L;
        }
        return room.deleteBot();
    }

    @Override
    public boolean isRoomEmpty() {
        return room.isRoomEmpty();
    }

    @Override
    public List<Long> getAllBotsId() {
        return room.getAllBotsId();
    }
}
