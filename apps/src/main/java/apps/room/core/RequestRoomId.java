package apps.room.core;

public class RequestRoomId {

    private int roomId;

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public int getRoomId() {
        return this.roomId;
    }
}