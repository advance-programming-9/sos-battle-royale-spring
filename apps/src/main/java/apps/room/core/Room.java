package apps.room.core;

import apps.bot.core.Bot;
import apps.game.core.Game;
import apps.game.core.arena.Arena;
import apps.game.core.arena.GridRequest;
import apps.room.core.base.RoomSimplified;
import apps.user.model.User;
import java.util.List;
import java.util.Random;

public class Room implements RoomSimplified {
    private final int roomId;
    private final String title;
    private Game game = new Game();

    public Room() {
        this("default");
    }

    public Room(String title) {
        this(Math.abs((new Random()).nextInt()) + 1, title);
    }

    public Room(int roomId, String title) {
        this.roomId = roomId;
        this.title = title;
    }

    @Override
    public Room enter(User user) {
        boolean successJoinGame = game.enter(user);
        if (successJoinGame) {
            return this;
        }
        return null;
    }

    @Override
    public int getRoomId() {
        return roomId;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public boolean getIsProtected() {
        return false;
    }

    public boolean start() {
        return game.start();
    }

    public boolean finish() {
        return game.finish();
    }

    public long getWhoIsNext() {
        return game.getWhoIsNext();
    }

    @Override
    public long click(GridRequest gridRequest) {
        int x = gridRequest.getX();
        int y = gridRequest.getY();
        char character = gridRequest.getCharacter();

        return game.click(x, y, character);
    }

    public Arena getArena() {
        return game.getArena();
    }

    public long getRoomMaster() {
        return game.getRoomMaster();
    }

    public int getCountPeople() {
        return game.getCountPeople();
    }

    public String getState() {
        return game.getState();
    }

    public List<Long> getUsers() {
        return game.getUsers();
    }

    @Override
    public boolean quit(User user) {
        return game.quit(user);
    }

    public void passTurn(long userId) {
        game.passTurn(userId);
    }

    public boolean enterBot(Bot bot) {
        return game.enterBot(bot);
    }

    public Long deleteBot() {
        return game.deleteBot();
    }
    
    public boolean isRoomEmpty() {
        return game.isRoomEmpty();
    }

    public List<Long> getAllBotsId() {
        return game.getAllBotsId();
    } 
}