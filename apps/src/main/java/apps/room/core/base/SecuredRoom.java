package apps.room.core.base;

import apps.bot.core.Bot;
import apps.room.core.Room;
import apps.room.core.RoomResponse;
import apps.user.model.User;
import java.util.List;

public interface SecuredRoom extends RoomSimplified {
    public RoomResponse getRoomResponse();

    public void passTurn(long userId);

    public Room enter(User user, String password);

    public boolean enterBot(User user, Bot bot);

    public long getIdWhoIsNext();

    public Long deleteBot(User user);

    public boolean isRoomEmpty();

    public List<Long> getAllBotsId();
}