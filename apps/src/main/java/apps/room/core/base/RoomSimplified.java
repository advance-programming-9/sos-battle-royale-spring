package apps.room.core.base;

import apps.game.core.arena.GridRequest;
import apps.room.core.Room;
import apps.user.model.User;

public interface RoomSimplified {
    public int getCountPeople();

    public int getRoomId();

    public String getTitle();

    public boolean getIsProtected();

    public Room enter(User user) throws Exception;

    public boolean quit(User user);

    public boolean start();

    public long click(GridRequest gridRequest);
}
