package apps.room.core;

import apps.bot.core.Bot;
import apps.game.core.arena.GridRequest;
import apps.room.core.base.SecuredRoom;
import apps.user.model.User;
import java.util.List;

public class RoomWithoutPassword implements SecuredRoom {
    private Room room;
    private RoomResponse roomResp;

    public RoomWithoutPassword(Room room) {
        this.room = room;
    }

    @Override
    public boolean getIsProtected() {
        return false;
    }

    @Override
    public Room enter(User user, String password) {
        return this.room.enter(user);
    }

    @Override
    public Room enter(User user) throws Exception {
        throw new Exception("invalid, use enter(User user, String password) instead!");
    }

    @Override
    public int getRoomId() {
        return room.getRoomId();
    }

    @Override
    public String getTitle() {
        return room.getTitle();
    }

    @Override
    public int getCountPeople() {
        return room.getCountPeople();
    }

    @Override
    public boolean quit(User user) {
        return room.quit(user);
    }

    @Override
    public RoomResponse getRoomResponse() {
        if (roomResp == null) {
            roomResp = new RoomResponse(room);
        }
        return roomResp;
    }

    @Override
    public boolean start() {
        return room.start();
    }

    @Override
    public long click(GridRequest gridRequest) {
        return room.click(gridRequest);
    }

    @Override
    public void passTurn(long userId) {
        room.passTurn(userId);
    }

    @Override
    public boolean enterBot(User user, Bot bot) {
        if (room.getRoomMaster() != user.getId()) {
            return false;
        }
        return room.enterBot(bot);
    }

    @Override
    public long getIdWhoIsNext() {
        return room.getWhoIsNext();
    }

    @Override
    public Long deleteBot(User user) {
        if (room.getRoomMaster() != user.getId()) {
            return -1L;
        }
        return room.deleteBot();
    }

    @Override
    public boolean isRoomEmpty() {
        return room.isRoomEmpty();
    }

    @Override
    public List<Long> getAllBotsId() {
        return room.getAllBotsId();
    }
}
