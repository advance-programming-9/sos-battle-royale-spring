package apps.room.service;

import apps.bot.repository.BotRepository;
import apps.room.core.CreateRoom;
import apps.room.core.Room;
import apps.room.core.RoomResponse;
import apps.room.core.base.SecuredRoom;
import apps.room.repository.RoomRepository;
import apps.room.validator.EnterRoomWithValidator;
import apps.scoreboard.core.Scoreboard;
import apps.user.model.User;
import apps.user.repository.MatchRepository;
import apps.user.repository.UserRepositoryJPA;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
public class RoomImpl implements AbsRoomImpl {

    @Autowired
    private final RoomRepository repo;

    @Autowired
    private final UserRepositoryJPA userRepositoryJPA;

    @Autowired
    private final MatchRepository matchRepository;

    @Autowired
    private final BotRepository botRepository;

    @Autowired
    private final Scoreboard scoreboard;

    @Autowired
    private final SimpMessagingTemplate simpMessagingTemplate;

    /**
     * contruct room services with given autowired.
     * @param roomRepository autorwired
     * @param userRepositoryJPA autorwired
     * @param matchRepository autorwired
     * @param simpMessagingTemplate autorwired
     * @param scoreboard autowired
     */
    public RoomImpl(
        RoomRepository roomRepository,
        UserRepositoryJPA userRepositoryJPA,
        MatchRepository matchRepository,
        Scoreboard scoreboard,
        SimpMessagingTemplate simpMessagingTemplate,
        BotRepository botRepository
    ) {
        this.repo = roomRepository;
        this.userRepositoryJPA = userRepositoryJPA;
        this.matchRepository = matchRepository;
        this.scoreboard = scoreboard;
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.botRepository = botRepository;
        EnterRoomWithValidator.setRoomRepository(roomRepository);
        RoomResponse.setUserRepositoryJPA(userRepositoryJPA,matchRepository,botRepository);
    }

    @Override
    public List<SecuredRoom> findAll() {
        return repo.getRooms();
    }

    @Override
    public RoomResponse enter(String username, RoomResponse roomResp, EnterRoomWithValidator val) {

        User user = userRepositoryJPA.findByUsername(username).get();
        String error = val.getError();

        if (error != null) {
            if (error.equals("Room doesn't exist")) {
                matchRepository.removeMatchId(user.getId());
            }
            roomResp.setError(error);
            return roomResp;
        }

        int roomId = val.getRoomId();
        boolean isInAMatch = matchRepository.isInAMatch(user.getId());
        boolean isInThisMatch = matchRepository.isInThisMatch(user.getId(), roomId);

        if (isInAMatch && !isInThisMatch) {
            roomResp.setError("IS_IN_A_MATCH");
            return roomResp;
        }

        val.enter(user);
        Room room = val.getRoom();
        roomResp.setRoom(val.getRoom());

        try {
            matchRepository.setMatch(user.getId(), room.getRoomId());
            simpMessagingTemplate.convertAndSend("/game/room/" + room.getRoomId(), roomResp);
            return roomResp;
        } catch (NullPointerException n) {
            roomResp.setError("WRONG_PASSWORD/ROOM_FULL");
            return roomResp;
        }
    }

    @Override
    public boolean delete(Integer id, String username) {

        User user = userRepositoryJPA.findByUsername(username).get();
        SecuredRoom securedRoom = repo.getRoomById(id);

        try {
            if (!securedRoom.quit(user)) {
                return false;
            }
            if (securedRoom.isRoomEmpty()) {
                repo.deleteRoomById(id);
                for (long botId: securedRoom.getAllBotsId()) {
                    matchRepository.removeMatchId(botId);
                    botRepository.deleteBotById(botId);
                }
            }
        } catch (NullPointerException n) {
            return false;
        }

        Long inGameScore = matchRepository.getInGameScore(user.getId());
        matchRepository.removeMatchId(user.getId());
        if (!securedRoom.isRoomEmpty()) {
            simpMessagingTemplate.convertAndSend(
                "/game/room/" + securedRoom.getRoomId(),
                securedRoom.getRoomResponse()
            );
        }
        user.setScore(user.getScore() + inGameScore);
        scoreboard.update(user);
        return true;
    }

    @Override
    public SecuredRoom create(String username, CreateRoom roomComp) {
        roomComp.create();
        SecuredRoom room = roomComp.getRoom();
        User user = userRepositoryJPA.findByUsername(username).get();
        room.enter(user, roomComp.getPassword());
        matchRepository.setMatch(user.getId(), room.getRoomId());
        repo.addRoom(room);
        return room;
    }
}
