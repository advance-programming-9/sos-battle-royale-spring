package apps.room.service;

import apps.room.core.CreateRoom;
import apps.room.core.RoomResponse;
import apps.room.core.base.SecuredRoom;
import apps.room.validator.EnterRoomWithValidator;
import java.util.List;

public interface AbsRoomImpl {
    public List<SecuredRoom> findAll();

    public RoomResponse enter(String username, RoomResponse roomResp, EnterRoomWithValidator valid);

    public boolean delete(Integer id, String username);

    public SecuredRoom create(String username, CreateRoom room);
}
