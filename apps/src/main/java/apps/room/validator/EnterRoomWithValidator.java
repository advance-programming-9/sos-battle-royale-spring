package apps.room.validator;

import apps.room.core.Room;
import apps.room.core.base.SecuredRoom;
import apps.room.repository.RoomRepository;
import apps.user.model.User;

public class EnterRoomWithValidator {
    private int roomId;
    private String error;
    private String password;
    private Room room;

    private static RoomRepository roomRepository;

    public static void setRoomRepository(RoomRepository roomRepository) {
        EnterRoomWithValidator.roomRepository = roomRepository;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * user can enter from this method.
     * @param user user to enter
     */
    public void enter(User user) {
        SecuredRoom securedRoom = roomRepository.getRoomById(roomId);
        if (securedRoom == null) {
            error = "Room doesn't exist";
            return;
        }
        Room room = securedRoom.enter(user, password);
        if (room == null) {
            error = "Password doesn't match";
            return;
        }
        this.room = room;
    }

    public String getError() {
        return error;
    }

    public Room getRoom() {
        return room;
    }
}