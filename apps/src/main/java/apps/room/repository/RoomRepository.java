package apps.room.repository;

import apps.room.core.base.SecuredRoom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;

@Repository
public class RoomRepository {

    private Map<Integer, SecuredRoom> roomRepository = new HashMap<>();

    public List<SecuredRoom> getRooms() {
        return new ArrayList<>(roomRepository.values());
    }

    public void addRoom(SecuredRoom securedRoom) {
        roomRepository.put(securedRoom.getRoomId(), securedRoom);
    }

    public SecuredRoom getRoomById(Integer roomId) {
        return roomRepository.get(roomId);
    }

    public void deleteRoomById(Integer roomId) {
        roomRepository.remove(roomId);
    }
}