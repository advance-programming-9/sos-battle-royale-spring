package apps.room.controller;

import apps.room.core.CreateRoom;
import apps.room.core.RequestRoomId;
import apps.room.core.RoomResponse;
import apps.room.core.base.SecuredRoom;
import apps.room.service.AbsRoomImpl;
import apps.room.validator.EnterRoomWithValidator;
import apps.user.auth.JwtAuthorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/room")
@CrossOrigin(origins = { "http://localhost:21483", "http://localhost:3000", "https://sos.ariqbasyar.id" })
public class RoomController {

    @Autowired
    private AbsRoomImpl roomService;

    /**
     * get mapping for list of rooms.
     * @param jwtAuthorization request header authorization
     * @return Response entity that contains list of rooms
     */
    @GetMapping
    public ResponseEntity findAll(
        @RequestHeader (name = "Authorization") JwtAuthorization jwtAuthorization
    ) {
        String error = jwtAuthorization.getError();
        if (error != null) {
            return ResponseEntity.badRequest().body(-1);
        }
        return new ResponseEntity<>(roomService.findAll(), HttpStatus.OK);
    }

    /**
     * get mapping to exit a room.
     * @param jwtAuthorization request header authorization
     * @param exitRoom input room id
     */
    @PostMapping("/exit")
    public ResponseEntity exitGameById(
        @RequestHeader (name = "Authorization") JwtAuthorization jwtAuthorization,
        @RequestBody RequestRoomId exitRoom
    ) {
        String error = jwtAuthorization.getError();
        if (error != null) {
            return ResponseEntity.badRequest().body(-1);
        }
        String username = jwtAuthorization.getUsername();
        if (!roomService.delete(exitRoom.getRoomId(),username)) {
            return ResponseEntity.badRequest().body(-1);
        }
        return ResponseEntity.ok(exitRoom.getRoomId());

    }

    /**
     * put mapping to create a room.
     * @param jwtAuthorization request header authorization
     * @param room component that needed to create a room
     */
    @PutMapping("/create")
    public ResponseEntity create(
        @RequestHeader (name = "Authorization") JwtAuthorization jwtAuthorization,
        @RequestBody CreateRoom room
    ) {
        String error = jwtAuthorization.getError();
        if (error != null) {
            return ResponseEntity.badRequest().body(-1);
        }
        SecuredRoom result = roomService.create(jwtAuthorization.getUsername(), room);
        return ResponseEntity.ok(result.getRoomId());
    }

    /**
     * post mapping to enter a room with given room validation.
     * @param jwtAuthorization request header authorization
     * @param enterRoom validator that needed to enter the room
     * @return Response entity bad request/ok response
     */
    @PostMapping("/enter")
    public ResponseEntity<RoomResponse> enter(
        @RequestHeader (name = "Authorization") JwtAuthorization jwtAuthorization,
        @RequestBody EnterRoomWithValidator enterRoom
    ) {
        RoomResponse roomResponse = new RoomResponse();
        String error = jwtAuthorization.getError();
        if (error != null) {
            roomResponse.setError(error);
            return ResponseEntity.badRequest().body(roomResponse);
        }

        String username = jwtAuthorization.getUsername();
        RoomResponse result = roomService.enter(username, roomResponse, enterRoom);
        if (result.getError() != null) {
            return ResponseEntity.badRequest().body(result);
        }
        result.setRoom(enterRoom.getRoom());
        return ResponseEntity.ok(result);
    }
}